# 0. Préparation de la machine

## Hôte 1

### Accès internet (via la carte NAT)

#### Carte NAT 

    [david@node1 ~]$ ip a
    [...]
    2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
        link/ether 08:00:27:95:83:9c brd ff:ff:ff:ff:ff:ff
        inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
           valid_lft 81398sec preferred_lft 81398sec
        inet6 fe80::a00:27ff:fe95:839c/64 scope link noprefixroute
           valid_lft forever preferred_lft forever
    [...]
    
Accès internet:

    [david@node1 ~]$ ping 8.8.8.8
    PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
    64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=24.2 ms

    

#### Route par défaut 
 
     [david@node1 ~]$ ip r s
    default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
    10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100
    10.101.1.0/24 dev enp0s8 proto kernel scope link src 10.101.1.11 metric 101

### Accès à un réseau local + Configuration DNS

Réseau local:

    [david@node1 ~]$ ip a
    [...]
    3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
        link/ether 08:00:27:87:02:68 brd ff:ff:ff:ff:ff:ff
        inet 10.101.1.11/24 brd 10.101.1.255 scope global noprefixroute enp0s8
           valid_lft forever preferred_lft forever
        inet6 fe80::a00:27ff:fe87:268/64 scope link noprefixroute
           valid_lft forever preferred_lft forever
           
Ping vers node2:
    
    [david@node1 ~]$ ping 10.101.1.12
    PING 10.101.1.12 (10.101.1.12) 56(84) bytes of data.
    64 bytes from 10.101.1.12: icmp_seq=1 ttl=64 time=0.614 ms

Modification de l'adresse du serveur DNS:

    [david@node1 ~]$ sudo nmcli connection modify enp0s8 ipv4.dns 1.1.1.1
    [david@node1 ~]$ sudo nmcli connection reload
    
Véfication:

    [david@node1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
    [...]
    DNS1=1.1.1.1


### Nom d'hôte

    [david@node1 ~]$ sudo hostnamectl set-hostname node1.tp1.b2
    [david@node1 ~]$ hostname
    node1.tp1.b2

    Ensuite "systemctl reboot"
    

## Hôte 2

### Accès internet (via la carte NAT)

#### Carte NAT 

    [david@node2 ~]$ ip a
    [...]
    2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:1c:fb:c6 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 80633sec preferred_lft 80633sec
    inet6 fe80::a00:27ff:fe1c:fbc6/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
    [...]
    
Accès internet:

    [david@node2 ~]$ ping 8.8.8.8
    PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
    64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=24.2 ms

    

#### Route par défaut 
 
     [david@node2 ~]$ ip r s
    default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
    10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100
    10.101.1.0/24 dev enp0s8 proto kernel scope link src 10.101.1.12 metric 101

### Accès à un réseau local + Configuration DNS

Réseau local:

    [david@node2 ~]$ ip a
    [...]
    3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:1d:86:50 brd ff:ff:ff:ff:ff:ff
    inet 10.101.1.12/24 brd 10.101.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe1d:8650/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
           
Ping vers node1:
    
    [david@node2 ~]$ ping 10.101.1.11
    PING 10.101.1.11 (10.101.1.11) 56(84) bytes of data.
    64 bytes from 10.101.1.11: icmp_seq=1 ttl=64 time=0.616 ms

Modification de l'adresse du serveur DNS:

    [david@node2 ~]$ sudo nmcli connection modify enp0s8 ipv4.dns 1.1.1.1
    [david@node2 ~]$ sudo nmcli connection reload
    
Véfication:

    [david@node2 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
    [...]
    DNS1=1.1.1.1


### Dig

    [david@node1 ~]$ dig ynov.com

l'IP qui correspond au nom demandé:

    [...]
    ;; ANSWER SECTION:
    ynov.com.               399     IN      A       92.243.16.143
    [...]
L'adresse IP du serveur qui vous a répondu:

    [...]
    ;; SERVER: 10.33.10.2#53(10.33.10.2)
    [...]

### Modification du fichier /etc/hosts

Hôte 1:
    
    [david@node1 ~]$ sudo vim /etc/hosts
    
    [...]
    Ligne à ajouter à la fin --->  10.101.1.12 node2 node2.tp2.b2
    
Vérifications:

    [david@node1 ~]$ ping node2
    PING node2 (10.101.1.12) 56(84) bytes of data.
    64 bytes from node2 (10.101.1.12): icmp_seq=1 ttl=64 time=0.738 ms

Hôte 2:
    
    [david@node2 ~]$ sudo vim /etc/hosts
    
    [...]
    Ligne à ajouter à la fin --->  10.101.1.11 node1 node2.tp2.b2
    
Vérifications:

    [david@node2 ~]$ ping node1
    PING node1 (10.101.1.11) 56(84) bytes of data.
    64 bytes from node1 (10.101.1.11): icmp_seq=1 ttl=64 time=0.738 ms



# I.Utilisateurs

## 1. Création et configuration

Hôte1:
    
    [david@node1 ~]$ sudo useradd -d /home/test_node1 -s /bin/bash test_node1
    [david@node1 ~]$ sudo passwd test_node1
    Changing password for user test_node1.
    New password:
    Retype new password:
    passwd: all authentication tokens updated successfully.
    
### Crétion du groupe et ajout de l'utilisateur créé

    [david@node1 ~]$ sudo groupadd admins
    [david@node1 ~]$ sudo usermod -a test_node1 -G admins
    


### Droits sudo
  
Autorisater le groupe "admins" à utiliser uniquement les commandes "useradd" et "userdell":

    [david@node1 ~]$ sudo vim /etc/sudoers.d/group_admins
    %admins         ALL=(ALL)       /usr/sbin/useradd, /usr/sbin/userdel

Vérifications:

  Connexion à test_node1, membre du groupe "admins":
  
    [david@node1 ~]$ su - test_node1
    Password:
    [test_node1@node1 ~]$

  Vérifications des privilèges autorisées:
  
    [test_node1@node1 ~]$ sudo useradd test
    [sudo] password for test_node1:
    [test_node1@node1 ~]$
    
    [test_node1@node1 ~]$ getent passwd | tail -1
    test:x:1002:1003::/home/test:/bin/bash

    --> Utilisateur "test" a bien été créé 
    
    [test_node1@node1 ~]$ sudo usermod test -d /home/final
    Sorry, user test_node1 is not allowed to execute '/sbin/usermod test -d /home/final' as root on node1.tp1.b2.
    
    --> La commande a échouée comme prévue
    
    [test_node1@node1 ~]$ sudo userdel test
    [test_node1@node1 ~]$ getent passwd | tail -1
    test_node1:x:1001:1001::/home/test_node1:/bin/bash
    
    --> Utilisateur "test" a bien été supprimé
    
    
## 2. SSH

Copie de la clé publique de Windows vers mon Node1:

    PS C:\WINDOWS\system32> type $env:C:\Users\rouss\.ssh\id_rsa.pub | ssh david@10.101.1.11 "cat >> .ssh/authorized_keys"
    david@10.101.1.11's password:
    PS C:\WINDOWS\system32>
    
Je me connecte à présent sur Node1 pour changer les permissions sur le fichier:

    PS C:\WINDOWS\system32> ssh david@10.101.1.11
    david@10.101.1.11's password:
    Activate the web console with: systemctl enable --now cockpit.socket

    Last login: Wed Sep 22 17:44:22 2021 from 10.101.1.1
    [david@node1 ~]$ sudo chmod 600 .ssh/authorized_keys
    
Je quitte ma connexion SSH et tente de me reconnecter:

    [david@node1 ~]$ exit
    logout
    Connection to 10.101.1.11 closed.
    
    PS C:\WINDOWS\system32> ssh david@10.101.1.11
    Activate the web console with: systemctl enable --now cockpit.socket

    Last login: Wed Sep 22 17:44:36 2021 from 10.101.1.1
    [david@node1 ~]$
    
    ---> Connexion sans utilisation de mot de passe !!
    
# II. Partitionnement

Créations des deux "physical volume":
    
    [david@node1 ~]$ sudo pvcreate /dev/sdb /dev/sdc
    Physical volume "/dev/sdb" successfully created.
    Physical volume "/dev/sdc" successfully created.
 
Agrégation de deux disques en "group volume":

    [david@node1 ~]$ sudo vgcreate  group_volume01 /dev/sdb /dev/sdc
    Volume group "group_volume01" successfully created
    
Création de 3 "logical volumes" de 1 Go chacun

    [david@node1 ~]$ sudo lvcreate -n logical_volume01 -L 1G group_volume01
      Logical volume "logical_volume01" created.
    [david@node1 ~]$ sudo lvcreate -n logical_volume02 -L 1G group_volume01
      Logical volume "logical_volume02" created.
    [david@node1 ~]$ sudo lvcreate -n logical_volume03 -L 1G group_volume01
      Logical volume "logical_volume03" created.
 
Formatage en ext4:

    [david@node1 ~]$ for fs in /dev/group_volume01/logical_volume0{1,2,3}; do sudo mkfs.ext4 $fs;
     done
    mke2fs 1.45.6 (20-Mar-2020)
    Creating filesystem with 262144 4k blocks and 65536 inodes
    Filesystem UUID: 6794e302-01ae-4573-8f06-7053cedc959c
    Superblock backups stored on blocks:
            32768, 98304, 163840, 229376

    Allocating group tables: done
    Writing inode tables: done
    Creating journal (8192 blocks): done
    Writing superblocks and filesystem accounting information: done

    mke2fs 1.45.6 (20-Mar-2020)
    Creating filesystem with 262144 4k blocks and 65536 inodes
    Filesystem UUID: fe8e92f3-5dee-4b0f-b3d0-f92612967465
    Superblock backups stored on blocks:
            32768, 98304, 163840, 229376

    Allocating group tables: done
    Writing inode tables: done
    Creating journal (8192 blocks): done
    Writing superblocks and filesystem accounting information: done

    mke2fs 1.45.6 (20-Mar-2020)
    Creating filesystem with 262144 4k blocks and 65536 inodes
    Filesystem UUID: 580314fc-6863-4aed-8a8a-fc773d101127
    Superblock backups stored on blocks:
            32768, 98304, 163840, 229376

    Allocating group tables: done
    Writing inode tables: done
    Creating journal (8192 blocks): done
    Writing superblocks and filesystem accounting information: done
  
Afin de monter les fichiers de façon permanente, il est préférable d'utiliser les UUID correspondant au fs ext4:

    [david@node1 ~]$ lsblk -fs
    [...]
    group_volume01-logical_volume01
    │                         ext4              6794e302-01ae-4573-8f06-7053cedc959c
    └─sdb                     LVM2_member       VXugqa-zoVJ-r3Xg-BlMR-Nje0-P2nz-cVeNU2
    group_volume01-logical_volume02
    │                         ext4              fe8e92f3-5dee-4b0f-b3d0-f92612967465
    └─sdb                     LVM2_member       VXugqa-zoVJ-r3Xg-BlMR-Nje0-P2nz-cVeNU2
    group_volume01-logical_volume03
    │                         ext4              580314fc-6863-4aed-8a8a-fc773d101127
    └─sdb                     LVM2_member       VXugqa-zoVJ-r3Xg-BlMR-Nje0-P2nz-cVeNU2
    
Création des points de montage:
    
    [david@node1 ~]$ sudo mkdir /mnt/part{1,2,3} -v
    [sudo] password for david:
    mkdir: created directory '/mnt/part1'
    mkdir: created directory '/mnt/part2'
    mkdir: created directory '/mnt/part3'
    
Configuration du montage permanent:
    
    [david@node1 ~]$ sudo vim /etc/fstab
        -- Ajouter les lignes suivantes à la fin du fichier ---
        
    UUID=6794e302-01ae-4573-8f06-7053cedc959c       /mnt/part1      ext4    defaults        0 0
    UUID=fe8e92f3-5dee-4b0f-b3d0-f92612967465       /mnt/part2      ext4    defaults        0 0
    UUID=580314fc-6863-4aed-8a8a-fc773d101127       /mnt/part3      ext4    defaults        0 0
    
Vérifications du fichier /etc/fstab:
    
    [david@node1 ~]$ sudo findmnt --verify
    Success, no errors or warnings detected
    
Montage:

    [david@node1 ~]$ sudo mount -a
    
Vérifications:

    [david@node1 ~]$ lsblk
    NAME                              MAJ:MIN RM  SIZE     
    [...]                                 
    ├─group_volume01-logical_volume01 253:2    0    1G  0 lvm  /mnt/part1
    ├─group_volume01-logical_volume02 253:3    0    1G  0 lvm  /mnt/part2
    └─group_volume01-logical_volume03 253:4    0    1G  0 lvm  /mnt/part3
    [...]
    
# III. Gestion de services

## 1. Interaction avec un service existant

Etat du service firewalld:

    [david@node1 ~]$ sudo systemctl is-active firewalld.service && sudo systemctl is-enabled firewalld.service
    active
    enabled
    
## 2. Création de service

    [david@node1 ~]$ sudo vim /etc/systemd/system/web.service
    [Unit]
    Description=Very simple web service

    [Service]
    ExecStart=/bin/python3 -m http.server 8888

    [Install]
    WantedBy=multi-user.target
    
Ouverture du port tcp 8888: 
    
    [david@node1 ~]$ sudo firewall-cmd --add-port=8888/tcp --permanent
    success

Démarrage du service:
    
    [david@node1 ~]$ sudo systemctl start web.service
    [david@node1 ~]$ sudo systemctl enable web.service
    Created symlink /etc/systemd/system/multi-user.target.wants/web.service → /etc/systemd/system/web.service.
    
Vérifications:

    [david@node1 ~]$ curl localhost:8888
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
    <html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Directory listing for /</title>
    </head>
    <body>
    <h1>Directory listing for /</h1>
    <hr>
    <ul>
    <li><a href="bin/">bin@</a></li>
    <li><a href="boot/">boot/</a></li>
    <li><a href="dev/">dev/</a></li>
    <li><a href="etc/">etc/</a></li>
    <li><a href="home/">home/</a></li>
    <li><a href="lib/">lib@</a></li>
    <li><a href="lib64/">lib64@</a></li>
    <li><a href="media/">media/</a></li>
    <li><a href="mnt/">mnt/</a></li>
    <li><a href="opt/">opt/</a></li>
    <li><a href="proc/">proc/</a></li>
    <li><a href="root/">root/</a></li>
    <li><a href="run/">run/</a></li>
    <li><a href="sbin/">sbin@</a></li>
    <li><a href="srv/">srv/</a></li>
    <li><a href="sys/">sys/</a></li>
    <li><a href="tmp/">tmp/</a></li>
    <li><a href="usr/">usr/</a></li>
    <li><a href="var/">var/</a></li>
    </ul>
    <hr>
    </body>
    </html>

## B. Modification de l'unité

Création de l'utilisateur web:

    [david@node1 ~]$ sudo useradd web
    
Création du répertoire à utiliser:

    [david@node1 ~]$ sudo mkdir /srv/web_directory

Modifications du service:

    [david@node1 ~]$ sudo vim /etc/systemd/system/web.service
    
    [...]
    [Service]
    ExecStart=/bin/python3 -m http.server 8888
    User=web
    WorkingDirectory=/srv/web_directory
    [...]
    
    [david@node1 ~]$ sudo systemctl daemon-reload
    [david@node1 ~]$ sudo systemctl restart web.service

Création d'un fichier:

    [david@node1 ~]$ sudo touch /srv/web_directory/test_file
    
Attribution du répertoire et fichier à l'utilisateur "web":
    
    [david@node1 ~]$ sudo chown -R web:web /srv
    
Vérifications:

    [david@node1 ~]$ curl localhost:8888
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
    <html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Directory listing for /</title>
    </head>
    <body>
    <h1>Directory listing for /</h1>
    <hr>
    <ul>
    <li><a href="test_file">test_file</a></li>
    </ul>
    <hr>
    </body>
    </html>
    

    
    