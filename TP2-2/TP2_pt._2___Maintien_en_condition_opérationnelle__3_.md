# TP2 pt. 2 : Maintien en condition opérationnelle

# I. Monitoring

## Setup

De nombreuses solutions de monitoring existent sur le marché. Nous, on va utiliser [Netdata](https://www.netdata.cloud/).  

Maintenant, vous êtes des techs. Alors la page qui vous intéresse encore plus, c'est [le dépôt git de la solution](https://github.com/netdata/netdata).

- le README.md y est souvent très complet
- présente la solution, et les étapes d'install
- fournit les liens vers la doc

🌞 **Setup Netdata**

  Sur web.tp2.linux:

    [root@web ~]#  bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
    --- Downloading static netdata binary: https://storage.googleapis.com/netd ata-nightlies/netdata-latest.gz.run ---
    [/tmp/netdata-kickstart-2of5iiVsLh]# curl -q -sSL --connect-timeout 10 --retry 3 --output /tmp/netdata-kickstart-2of5iiVsLh/sha256sum.txt https://storage.googleapis.com/netdata-nightlies/sha256sums.txt
    OK
    [...]

    [root@web ~]# exit
    logout

    [david@web ~]$

Sur db.tp2.linux:

    [root@db ~]#  bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
    --- Downloading static netdata binary: https://storage.googleapis.com/netd ata-nightlies/netdata-latest.gz.run ---
    [/tmp/netdata-kickstart-2of5iiVsLh]# curl -q -sSL --connect-timeout 10 --retry 3 --output /tmp/netdata-kickstart-2of5iiVsLh/sha256sum.txt https://storage.googleapis.com/netdata-nightlies/sha256sums.txt
    OK
    [...]

    [root@db ~]# exit
    logout

    [david@db ~]$


🌞 **Manipulation du *service* Netdata**

Déterminer s'il est actif, et s'il est paramétré pour démarrer au boot de la machine
si ce n'est pas le cas, faites en sorte qu'il démarre au boot de la machine
    
Sur web.tp2.linux:
    
    [david@web ~]$ sudo systemctl is-enabled netdata.service
    [sudo] password for david:
    enabled
    
    [david@web ~]$ sudo systemctl is-active netdata.service
    active

Sur db.tp2.linux:

    [david@db ~]$ sudo systemctl is-active netdata.service
    [sudo] password for david:
    active

    david@db ~]$ sudo systemctl is-enabled netdata.service
    enabled


Déterminer à l'aide d'une commande `ss` sur quel port Netdata écoute

Sur web.tp2.linux:

    [david@web ~]$ sudo  ss -naltp
    [...]
    LISTEN    0         128                0.0.0.0:19999             0.0.0.0:*        users:(("netdata",pid=2065,fd=5))
    [...]
    
Sur db.tp2.linux:

    [...]
    [david@db ~]$ sudo  ss -naltp
    LISTEN    0         128                0.0.0.0:19999             0.0.0.0:*        users:(("netdata",pid=2065,fd=5))
    [...]

Autoriser ce port dans le firewall

    Sur web.tp2.linux:
    
    [david@web ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
    success

    [david@web ~]$ sudo firewall-cmd --reload
    success

Sur db.tp2.linux:

    [david@db ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
    success

    [david@db ~]$ sudo firewall-cmd --reload
    success


🌞 **Setup Alerting**

ajustez la conf de Netdata pour mettre en place des alertes Discord


Depuis l'hôte web:

    [david@web ~]$ sudo vim /opt/netdata/etc/netdata/health_alarm_notify.conf
    
    #Sending discord notifications

    # note: multiple recipients can be given like this:
    #                  "CHANNEL1 CHANNEL2 ..."

    # enable/disable sending discord notifications
    SEND_DISCORD="YES"

    # Create a webhook by following the official documentation -
    # https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
    DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/897107407312945173/axfE3MJDZKvH6aEFo1HhdxrQ32u9o9lFgdT0-aQ9nd0IAEoqv8UIXl-NuFUHPcVWSJeu"

    # if a role's recipients are not configured, a notification will be send to
    # this discord channel (empty = do not send a notification for unconfigured
    # roles):
    DEFAULT_RECIPIENT_DISCORD="alarms"
    
Depuis l'hôte db:

    [david@db ~]$ [david@db ~]$ sudo vim /opt/netdata/etc/netdata/health_alarm_notify.conf
    #Sending discord notifications

    # note: multiple recipients can be given like this:
    #                  "CHANNEL1 CHANNEL2 ..."

    # enable/disable sending discord notifications
    SEND_DISCORD="YES"

    # Create a webhook by following the official documentation -
    # https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
    DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/897107407312945173/axfE3MJDZKvH6aEFo1HhdxrQ32u9o9lFgdT0-aQ9nd0IAEoqv8UIXl-NuFUHPcVWSJeu"

    # if a role's recipients are not configured, a notification will be send to
    # this discord channel (empty = do not send a notification for unconfigured
    # roles):
    DEFAULT_RECIPIENT_DISCORD="alarms"


vérifiez le bon fonctionnement de l'alerting sur Discord

Sur web:

    [david@web ~]$ sudo su -s /bin/bash netdata
    bash-4.4$ export NETDATA_ALARM_NOTIFY_DEBUG=1
    bash-4.4$ /opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test

    # SENDING TEST WARNING ALARM TO ROLE: sysadmin
    [...]
    # OK

Sur db:

    [david@wdb ~]$ sudo su -s /bin/bash netdata
    bash-4.4$ export NETDATA_ALARM_NOTIFY_DEBUG=1
    bash-4.4$ /opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test

    # SENDING TEST WARNING ALARM TO ROLE: sysadmin
    [...]
    # OK
    
Alertes remontées sur Discord:

Sur web:

    netdata on web.tp2.linux
    BOT
     — Today at 4:13 PM
    web.tp2.linux needs attention, test.chart (test.family), test alarm = new value
    [...]

Sur db:

    netdata on db.tp2.linux
    BOT
     — Today at 4:16 PM
    db.tp2.linux needs attention, test.chart (test.family), test alarm = new value

🌞 **Config alerting**

![stress test](./pics/stress-test.jpg)

# II. Backup
 
## Partage NFS

🌞 **Setup environnement**

créer un dossier `/srv/backup/`

il existera un partage NFS pour chaque machine (principe du moindre privilège)

    [david@backup ~]$ sudo mkdir /srv/backup/
    [david@backup ~]$ sudo chown -R david:david /srv/backup/
    [sudo] password for david:
    [david@backup ~]$

🌞 **Setup partage NFS**

    [david@backup ~]$ sudo dnf install nfs-utils -y
    Last metadata expiration check: 0:00:06 ago on Mon 11 Oct 2021 04:42:20 PM CEST.
    Dependencies resolved.
    [...]

    [david@backup ~]$ sudo vim /etc/idmapd.conf
    ...]
    Domain = tp2.linux
    [...]

    [david@backup ~]$ sudo mkdir -p /srv/backup/{web,db}.tp2.linux

    [david@backup ~]$ sudo vim /etc/exports
    /srv/backup/web.tp2.linux web(rw, no_root_squash) /srv/backup/db.tp2.linux 
    db(rw, no_root_squash)
    
    [david@backup ~]$ sudo systemctl start nfs-server.service
    [david@backup ~]$ sudo systemctl enable nfs-server.service
    Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
      
    [david@backup ~]$ sudo firewall-cmd --add-service={nfs3,mountd,rpc-bind} --permanent
    success
    [david@backup ~]$ sudo firewall-cmd --reload
    success
    
🌞 **Setup points de montage sur `web.tp2.linux`**

monter le dossier `/srv/backups/web.tp2.linux` du serveur NFS dans le dossier `/srv/backup/` du serveur Web

    [david@web ~]$ sudo mkdir /srv/backup
    [sudo] password for david:
  
 faites en sorte que cette partition se monte automatiquement grâce au fichier `/etc/fstab`
 
    [david@web ~]$ sudo vim /etc/fstab
    [...]
    backup:/srv/backup/web.tp2.linux /srv/backup    nfs     defaults        0 0
    [...]
    
   
vérifier:
  avec une commande `mount` que la partition est bien montée
  
    [david@web ~]$ sudo mount /srv/backup/ -v
    mount.nfs: timeout set for Tue Oct 12 14:10:23 2021
    mount.nfs: trying text-based options 'vers=4.2,addr=10.102.1.13,clientaddr=10.102.1.11'
  
  avec une commande `df -h` qu'il reste de la place
  
      [david@web ~]$ df -h
      [...]
      backup:/srv/backup/web.tp2.linux  6.2G  2.0G  4.2G  33% /srv/backup
      
  avec une commande `touch` que vous avez le droit d'écrire dans cette partition
  
    [david@web ~]$ touch /srv/backup/web_test
    [david@web ~]$ ls /srv/backup/
    web_test

Sur db.tp2.linux:
 
    
Démarrage du service nfs:

    [david@db srv]$ sudo systemctl start nfs-server.service
    [david@db srv]$ sudo systemctl enable nfs-server.service
    Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.

Création du répertoire de partage:

    [david@db srv]$ sudo mkdir /srv/backup
    
Montage du partage:

    [david@db ~]$ sudo vim /etc/fstab
    [...]
    backup:/srv/backup/db.tp2.linux /srv/backup    nfs     defaults        0 0

    [david@db ~]$ sudo findmnt --verify
    Success, no errors or warnings detected

    [david@db backup]$ sudo mount /srv/backup/     

🌟 **BONUS** : partitionnement avec LVM

ajoutez un disque à la VM `backup.tp2.linux`
    
    [david@backup ~]$ lsblk
    NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
    sda           8:0    0    8G  0 disk
    ├─sda1        8:1    0    1G  0 part /boot
    └─sda2        8:2    0    7G  0 part
      ├─rl-root 253:0    0  6.2G  0 lvm  /
      └─rl-swap 253:1    0  820M  0 lvm  [SWAP]
    sdb           8:16   0    8G  0 disk
    sr0          11:0    1 58.3M  0 rom
    sr1          11:1    1  1.9G  0 rom

utilisez LVM pour créer une nouvelle partition (5Go ça ira)

    [david@backup ~]$ sudo pvcreate /dev/sdb
    [sudo] password for david:
      Physical volume "/dev/sdb" successfully created.
      
    [david@backup ~]$ sudo vgcreate vg_backup /dev/sdb
      Volume group "vg_backup" successfully created
      
    [david@backup ~]$ sudo lvcreate -L 5g vg_backup -n lv_backup
      Logical volume "lv_backup" created.
      
    [david@backup ~]$ sudo mkfs.ext4 /dev/mapper/vg_backup-lv_backup
    mke2fs 1.45.6 (20-Mar-2020)
    Creating filesystem with 1310720 4k blocks and 327680 inodes
      
    [david@backup ~]$ sudo udevadm settle
   
    
monter automatiquement cette partition au démarrage du système à l'aide du fichier `/etc/fstab`

    [david@backup ~]$ lsblk --fs
    [...]
    sdb                   LVM2_member                      852XIj-FmvH-e2NE-BL1V-pHlP-6Kb6-k1Vt6f
    └─vg_backup-lv_backup ext4                             bb650600-6c7a-4bc7-9c7d-e52f900cc267
    [...]
    
    [david@backup ~]$ sudo vim /etc/fstab
    [...]
    UUID=bb650600-6c7a-4bc7-9c7d-e52f900cc267 /srv/backup   ext4    defaults        0 0
      
    [david@backup ~]$ sudo findmnt --verify
  Success, no errors or warnings detected

cette nouvelle partition devra être montée sur le dossier `/srv/backup/`

    [david@backup ~]$ sudo mount /srv/backup/ -v
    mount: /srv/backup does not contain SELinux labels.
          You just mounted an file system that supports labels which does not
          contain labels, onto an SELinux box. It is likely that confined
          applications will generate AVC messages and not be allowed access to
          this file system.  For more details see restorecon(8) and mount(8).
    mount: /dev/mapper/vg_backup-lv_backup mounted on /srv/backup.

## Backup de fichiers

🌞 **Rédiger le script de backup `/srv/tp2_backup.sh`**

📁 **Fichier `/srv/tp2_backup.sh`**

  https://gitlab.com/davidroussat/b2-linux/-/blob/main/Configurations/tp2_backup.sh



🌞 **Tester le bon fonctionnement**

exécuter le script sur le dossier de votre choix
prouvez que la backup s'est bien exécutée

    [david@web srv]$ sudo mkdir -p test/testons_dir
    [david@web srv]$ sudo ./tp2_backup.sh backup/ test/
    sending incremental file list
    tp2_backup_211015_141127.tar.gz

    sent 257 bytes  received 43 bytes  600.00 bytes/sec
    total size is 140  speedup is 0.47
    /srv

**tester de restaurer les données**
  récupérer l'archive générée, et vérifier son contenu
  
      [david@web backup]$ tar xzfv tp2_backup_211015_141127.tar.gz
    srv/test/
    srv/test/testons_dir/
  
      

🌟 **BONUS**

- faites en sorte que votre script ne conserve que les 5 backups les plus récentes après le `rsync`
- faites en sorte qu'on puisse passer autant de dossier qu'on veut au script : `./tp2_backup.sh <DESTINATION> <DOSSIER1> <DOSSIER2> <DOSSIER3>...` et n'obtenir qu'une seule archive
- utiliser [Borg](https://borgbackup.readthedocs.io/en/stable/) plutôt que `rsync`

## 4. Unité de service

### A. Unité de service

🌞 **Créer une *unité de service*** pour notre backup

- c'est juste un fichier texte hein
- doit se trouver dans le dossier `/etc/systemd/system/`
- doit s'appeler `tp2_backup.service`
- le contenu :


      [david@web ~]$ sudo vim /etc/systemd/system/tp2_backup.service
      [sudo] password for david:
      [Unit]
      Description=Our own lil backup service (TP2)

      [Service]
      ExecStart=/srv/tp2_backup.sh /srv/backup/backup.tp2.linux /srv/test_unit
      Type=oneshot
      RemainAfterExit=no

      [Install]
      WantedBy=multi-user.target

      [david@web ~]$ sudo systemctl start tp2_backup.service
      [david@web ~]$ sudo systemctl enable tp2_backup.service
      [david@web ~]$

> Pour les tests, sauvegardez le dossier de votre choix, peu importe lequel.

🌞 **Tester le bon fonctionnement**

- n'oubliez pas d'exécuter `sudo systemctl daemon-reload` à chaque ajout/modification d'un *service*
- essayez d'effectuer une sauvegarde avec `sudo systemctl start backup`
- prouvez que la backup s'est bien exécutée
 vérifiez la présence de la nouvelle archive
  
        [david@web srv]$ sudo mkdir -p test_unit/test1
      
        [david@web srv]$ sudo systemctl daemon-reload
        [david@web srv]$ sudo systemctl start tp2_backup.service
        [david@web srv]$ sudo systemctl enable tp2_backup.service
        Created symlink /etc/systemd/system/multi-user.target.wants/tp2_backup.service → /etc/systemd/system/tp2_backup.service.
        
        
        [david@web srv]$ cd backup/
        [david@web backup]$ ls
        tp2_backup_211015_141805.tar.gz
        
        [david@web backup]$ tar xzfv tp2_backup_211015_141805.tar.gz
        srv/test_unit/
        srv/test_unit/test1
        [david@web backup]$ cd srv/test_unit/
        [david@web test_unit]$ ls
        test1

### B. Timer

Un *timer systemd* permet l'exécution d'un *service* à intervalles réguliers.

🌞 **Créer le *timer* associé à notre `tp2_backup.service`**

- toujours juste un fichier texte
- dans le dossier `/etc/systemd/system/` aussi
- fichier `tp2_backup.timer`
contenu du fichier :

    
      [david@web ~]$ sudo vim /etc/systemd/system/tp2_backup.timer
      [Unit]
      Description=Periodically run our TP2 backup script
      Requires=tp2_backup.service

      [Timer]
      Unit=tp2_backup.service
      OnCalendar=*-*-* *:*:00

      [Install]
      WantedBy=timers.target

🌞 **Activez le timer**

- démarrer le *timer* : `sudo systemctl start tp2_backup.timer`
- activer le au démarrage avec une autre commande `systemctl`

      [david@web ~]$ sudo systemctl start tp2_backup.timer
      [david@web ~]$ sudo systemctl enable tp2_backup.timer

      Created symlink /etc/systemd/system/timers.target.wants/tp2_backup.timer → /etc/systemd/system/tp2_backup.timer.


- prouver que...
  - le *timer* est actif actuellement
        
        [david@web ~]$ sudo systemctl is-active tp2_backup.timer
        [sudo] password for david:
        active
    
    
  - qu'il est paramétré pour être actif dès que le système boot

        [david@web ~]$ sudo systemctl is-enabled tp2_backup.timer    
enabled

🌞 **Tests !**

- vérifiez que la backup s'exécute correctement

---

    [david@web backup]$ pwd
    /srv/backup
    [david@web backup]$ date
    Thu Oct 14 14:36:06 CEST 2021   
    [david@web backup]$ ls
    tp2_backup_211014_143209.tar.gz  tp2_backup_211014_143441.tar.gz  tp2_backup_211014_143641.tar.gz
    tp2_backup_211014_143341.tar.gz  tp2_backup_211014_143541.tar.gz

### C. Contexte

🌞 **Faites en sorte que...**

- votre backup s'exécute sur la machine 
- `web.tp2.linux`
- - le dossier sauvegardé est celui qui contient le site NextCloud (quelque part dans `/var/`)

- la destination est le dossier NFS monté depuis le serveur `backup.tp2.linux`


      [david@web ~]$ sudo vim /etc/systemd/system/tp2_backup.service
      [...]
      [Service]
      ExecStart=/srv/tp2_backup.sh /srv/backup/ /var/www/sub-domains
      [...]


- la sauvegarde s'exécute tous les jours à 03h15 du matin

      [david@web ~]$ sudo vim /etc/systemd/system/tp2_backup.timer
      [sudo] password for david:

      [Unit]
      Description=Periodically run our TP2 backup script
      Requires=tp2_backup.service

      [Timer]
      Unit=tp2_backup.service
      OnCalendar=*-*-* 3:15:00

      [Install]
      WantedBy=timers.target
    
      [david@web ~]$ sudo systemctl daemon-reload

- prouvez avec la commande `sudo systemctl list-timers` que votre *service* va bien s'exécuter la prochaine fois qu'il sera 03h15

            [david@web ~]$ sudo systemctl list-timers | grep tp2
        Fri 2021-10-15 03:15:00 CEST  11h left   Thu 2021-10-14 15:53:41 CEST  17min ago    tp2_backup.timer             tp2_backup.service

📁 **Fichier `/etc/systemd/system/tp2_backup.timer`**  

  https://gitlab.com/davidroussat/b2-linux/-/blob/main/Configurations/tp2_backup.timer

📁 **Fichier `/etc/systemd/system/tp2_backup.service`**

  https://gitlab.com/davidroussat/b2-linux/-/blob/main/Configurations/tp2_backup.service

## 5. Backup de base de données

Sauvegarder des dossiers c'est bien. Mais sauvegarder aussi les bases de données c'est mieux.

🌞 **Création d'un script `/srv/tp2_backup_db.sh`**

Configuration du fichier pour avoir stocker le mot de passe sans avoir à le taper dans le script:

    [david@db ~]$ vim ~/.my.cnf
    [mysqldump]
    password=meow

    [david@db ~]$ sudo chmod 600 ~/.my.cnf
    [sudo] password for david:
    [david@db ~]$
  
  
📁 **Fichier `/srv/tp2_backup_db.sh`**  

  https://gitlab.com/davidroussat/b2-linux/-/blob/main/Configurations/tp2_backup_db.sh

🌞 **Restauration**

- tester la restauration de données

      [david@db backup]$ mysql -u root -p nextcloud < home/david/database.sql
      Enter password:
      [david@db backup]$

🌞 ***Unité de service***

      [david@db backup]$ sudo systemctl start tp2_backup_db.timer
      [david@db backup]$ sudo systemctl enable tp2_backup_db.timer
      Created symlink /etc/systemd/system/timers.target.wants/tp2_backup_db.timer → /etc/systemd/system/tp2_backup_db.timer.
    
- prouvez le bon fonctionnement du *service* ET du *timer*

      [david@db backup]$ sudo systemctl list-timers
      [...]
      Sun 2021-10-17 03:30:00 CEST  16h left n/a                           n/a      tp2_backup_db.timer          tp2_backup_db.service
      [...]

    [david@db ~]$ date
    Sun Oct 17 18:21:35 CEST 2021
    [david@db ~]$ sudo systemctl restart tp2_backup_db.service
    [david@db ~]$ ls /srv/backup
    tp2_backup_db_211017_182141.tar.gz


📁 **Fichier `/etc/systemd/system/tp2_backup_db.timer`**  

  https://gitlab.com/davidroussat/b2-linux/-/blob/main/Configurations/tp2_backup_db.timer

📁 **Fichier `/etc/systemd/system/tp2_backup_db.service`**

  https://gitlab.com/davidroussat/b2-linux/-/blob/main/Configurations/tp2_backup_db.service


# III. Reverse Proxy

## Setup simple

🌞 **Installer NGINX**

- vous devrez d'abord installer le paquet `epel-release` avant d'installer `nginx`
  - EPEL c'est des dépôts additionnels pour Rocky

      [david@front ~]$ sudo dnf install epel-release -y
           
  - NGINX n'est pas présent dans les dépôts par défaut que connaît Rocky

      [david@front ~]$ sudo dnf install nginx -y
      Extra Packages for Enterprise Linux 8 - x86_64                                          372 kB/s |  10 MB     00:28
      Last metadata expiration check: 0:00:16 ago on Sat 16 Oct 2021 03:18:52 PM CEST.
      Dependencies resolved.
      [...]


🌞 **Tester !**

- lancer le *service* `nginx`
- le paramétrer pour qu'il démarre seul quand le système boot

      [david@front ~]$ sudo systemctl start nginx.service
      [david@front ~]$ sudo systemctl enable nginx.service
      Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
      [david@front ~]$

- repérer le port qu'utilise NGINX par défaut, pour l'ouvrir dans le firewall

      [david@front ~]$ sudo ss -naltp | grep nginx
      LISTEN 0      128          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=29473,fd=8),("nginx",pid=29472,fd=8))
      LISTEN 0      128             [::]:80           [::]:*    users:(("nginx",pid=29473,fd=9),("nginx",pid=29472,fd=9))
      
      [david@front ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
      success
      [david@front ~]$ sudo firewall-cmd --reload
      success
    
- vérifier que vous pouvez joindre NGINX avec une commande `curl` depuis votre PC

🌞 **Explorer la conf par défaut de NGINX**

- repérez l'utilisateur qu'utilise NGINX par défaut
    
      [david@front nginx]$ vim nginx.conf
      [...]
      user nginx;
      [...]

- dans la conf NGINX, on utilise le mot-clé `server` pour ajouter un nouveau site
  - repérez le bloc `server {}` dans le fichier de conf principal

        server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  _;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        location / {
        }

        error_page 404 /404.html;
            location = /40x.html {
        }

        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
    }
        
- par défaut, le fichier de conf principal inclut d'autres fichiers de conf
  - mettez en évidence ces lignes d'inclusion dans le fichier de conf principal

        include /usr/share/nginx/modules/*.conf;
        include             /etc/nginx/mime.types;
        include /etc/nginx/conf.d/*.conf;

        include /etc/nginx/default.d/*.conf;

🌞 **Modifier la conf de NGINX**

- pour que ça fonctionne, le fichier `/etc/hosts` de la machine **DOIT** être rempli correctement, conformément à la **[📝**checklist**📝](#checklist)

  [david@front ~]$ sudo vim /etc/hosts
  10.102.1.11     web.tp2.linux           web
  10.102.1.12     db.tp2.linux            db
  10.102.1.13     backup.tp2.linux        backup
  10.102.1.14     front.tp2.linux         front

  [david@front ~]$ sudo vim /etc/nginx/conf.d/web.tp2.linux.conf
  server {
        listen 80;

        server_name web.tp2.linux; 

        location / {
                proxy_pass http://web.tp2.linux;
         }
  }


## 3. Bonus HTTPS


🌟 **Générer la clé et le certificat pour le chiffrement**

    [david@front ~]$ openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt
  Generating a RSA private key

    [david@front ~]$ sudo mv server.key /etc/pki/tls/private/web.tp2.linux.key
    [sudo] Mot de passe de david : 
    [david@front ~]$ sudo mv server.crt /etc/pki/tls/certs/web.tp2.linux.crt

    [david@front ~]$ sudo chown root:root /etc/pki/tls/private/web.tp2.linux.key
    [david@front ~]$ sudo chown root:root /etc/pki/tls/certs/web.tp2.linux.crt
    [david@front ~]$ sudo chmod 400 /etc/pki/tls/private/web.tp2.linux.key 
    [david@front ~]$ sudo chmod 644 /etc/pki/tls/certs/web.tp2.linux.crt 


🌟 **Modifier la conf de NGINX**

    [david@front ~]$ sudo dnf install nginx mod_ssl
    [sudo] Mot de passe de david : 
    Dernière vérification de l’expiration des métadonnées effectuée il y a 1 day, 14:50:16 le dim. 17 oct. 2021   15:50:09 CEST.
    [...]

  [david@front ~]$ sudo vim /etc/nginx/conf.d/web.tp2.linux.conf 
  server {
    listen 443 ssl http2 defaut_server;

    server_name web.tp2.linux; 

    ssl_certificate "/etc/pki/tls/certs/web.tp2.linux.crt";
    ssl_certificate_key "/etc/pki/tls/private/web.tp2.linux.key";
    ssl_session_cache shared:SSL:1m;
    ssl_session_timeout  10m;
    ssl_ciphers PROFILE=SYSTEM;
    ssl_prefer_server_ciphers on;

    location / {
        proxy_pass http://web.tp2.linux;
    }
}

  [david@front ~]$ sudo firewall-cmd --add-port=443/tcp --permanent 
  success
  [david@front ~]$ sudo firewall-cmd --reload 
  success

# IV. Firewalling

### A. Base de données

🌞 **Restreindre l'accès à la base de données `db.tp2.linux`**

- seul le serveur Web doit pouvoir joindre la base de données sur le port 3306/tcp
- vous devez aussi autoriser votre accès SSH
- n'hésitez pas à multiplier les zones (une zone `ssh` et une zone `db` par exemple)

> Quand vous faites une connexion SSH, vous la faites sur l'interface Host-Only des VMs. Cette interface est branchée à un Switch qui porte le nom du Host-Only. Pour rappel, votre PC a aussi une interface branchée à ce Switch Host-Only.  
C'est depuis cette IP que la VM voit votre connexion. C'est cette IP que vous devez autoriser dans le firewall de votre VM pour SSH.

      [david@db ~]$ sudo firewall-cmd --set-default-zone=drop 
      success
      [david@db ~]$ sudo firewall-cmd --permanent --new-zone=ssh
      success
      [david@db ~]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent  
      success
      [david@db ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent 
      success

      [david@db ~]$ sudo firewall-cmd --permanent --new-zone=db
      success
      [david@db ~]$ sudo firewall-cmd --zone=db --add-port=3306/tcp --permanent 
      [david@db ~]$ sudo firewall-cmd --zone=db --add-source=10.102.1.11/32 --permanent 
      success

      [david@db ~]$ sudo firewall-cmd --reload 
      success


🌞 **Montrez le résultat de votre conf avec une ou plusieurs commandes `firewall-cmd`**

- `sudo firewall-cmd --get-active-zones`

    [david@db ~]$ sudo firewall-cmd --get-active-zones 
    db
      sources: 10.102.1.11/32
    drop
      interfaces: enp0s8 enp0s3
    ssh
      sources: 10.102.1.1/32


- `sudo firewall-cmd --get-default-zone`

      [david@db ~]$ sudo firewall-cmd --get-default-zone 
      drop

- `sudo firewall-cmd --list-all --zone=?`

      [david@db ~]$ sudo firewall-cmd --list-all --zone=ssh
    ssh (active)
      target: default
      icmp-block-inversion: no
      interfaces: 
      sources: 10.102.1.1/32
      services: 
      ports: 22/tcp
      protocols: 
      masquerade: no
      forward-ports: 
      source-ports: 
      icmp-blocks: 
      rich rules: 

      [david@db ~]$ sudo firewall-cmd --list-all --zone=db
    db (active)
      target: default
      icmp-block-inversion: no
      interfaces: 
      sources: 10.102.1.11/32
      services: 
      ports: 3306/tcp
      protocols: 
      masquerade: no
      forward-ports: 
      source-ports: 
      icmp-blocks: 
      rich rules: 

      [david@db ~]$ sudo firewall-cmd --list-all --zone=drop
    drop (active)
      target: DROP
      icmp-block-inversion: no
      interfaces: enp0s3 enp0s8
      sources: 
      services: 
      ports: 
      protocols: 
      masquerade: no
      forward-ports: 
      source-ports: 
      icmp-blocks: 
      rich rules:

### B. Serveur Web

🌞 **Restreindre l'accès au serveur Web `web.tp2.linux`**

- seul le reverse proxy `front.tp2.linux` doit accéder au serveur web sur le port 80
- n'oubliez pas votre accès SSH

      [david@web ~]$ sudo firewall-cmd --set-default-zone=drop 
      success
      [david@web ~]$ sudo firewall-cmd --permanent --new-zone=ssh
      success
      [david@web ~]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent  
      success
      [david@web ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent 
      success

      [david@web ~]$ sudo firewall-cmd --permanent --new-zone=web
      success
      [david@web ~]$ sudo firewall-cmd --zone=web --add-port=80/tcp --permanent 
      [david@web ~]$ sudo firewall-cmd --zone=web --add-source=10.102.1.14/32 --permanent 
      success

      [david@web ~]$ sudo firewall-cmd --reload 
      success

🌞 **Montrez le résultat de votre conf avec une ou plusieurs commandes `firewall-cmd`**

  - `sudo firewall-cmd --get-active-zones`

      [david@web ~]$ sudo firewall-cmd --get-active-zones 
      drop
        interfaces: enp0s8 enp0s3
      ssh
        sources: 10.102.1.1/32
      web
        sources: 10.102.1.14/32


- `sudo firewall-cmd --get-default-zone`

      [david@web ~]$ sudo firewall-cmd --get-default-zone 
      drop

- `sudo firewall-cmd --list-all --zone=?`

      [david@web ~]$ sudo firewall-cmd --list-all --zone=ssh
    ssh (active)
      target: default
      icmp-block-inversion: no
      interfaces: 
      sources: 10.102.1.1/32
      services: 
      ports: 22/tcp
      protocols: 
      masquerade: no
      forward-ports: 
      source-ports: 
      icmp-blocks: 
      rich rules: 

      [david@web ~]$ sudo firewall-cmd --list-all --zone=web
    web (active)
      target: default
      icmp-block-inversion: no
      interfaces: 
      sources: 10.102.1.14/32
      services: 
      ports: 80/tcp
      protocols: 
      masquerade: no
      forward-ports: 
      source-ports: 
      icmp-blocks: 
      rich rules: 

      [david@web ~]$ sudo firewall-cmd --list-all --zone=drop
    drop (active)
      target: DROP
      icmp-block-inversion: no
      interfaces: enp0s3 enp0s8
      sources: 
      services: 
      ports: 
      protocols: 
      masquerade: no
      forward-ports: 
      source-ports: 
      icmp-blocks: 
      rich rules:  


### C. Serveur de backup

🌞 **Restreindre l'accès au serveur de backup `backup.tp2.linux`**

- seules les machines qui effectuent des backups doivent être autorisées à contacter le serveur de backup *via* NFS
- n'oubliez pas votre accès SSH

      [david@backup ~]$ sudo firewall-cmd --set-default-zone=drop 
      success  
      [david@backup ~]$ sudo firewall-cmd --permanent --new-zone=ssh
      success
      [david@backup ~]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent  
      success
      [david@backup ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent 
      success

      [david@backup ~]$ sudo firewall-cmd --permanent --new-zone=nfs
      success
      [david@backup ~]$ sudo firewall-cmd --zone=nfs --add-port=111/tcp --add-port=111/udp --add-port=2049/tcp --add-port=2049/udp --permanent  
      [david@backup ~]$ sudo firewall-cmd --zone=nfs --add-source=10.102.1.11/32 --add-source=10.102.1.12/32 --permanent 
      success

      [david@backup ~]$ sudo firewall-cmd --reload 
      success

🌞 **Montrez le résultat de votre conf avec une ou plusieurs commandes `firewall-cmd`**

      [david@backup ~]$ sudo firewall-cmd --get-active-zones 
    drop
      interfaces: enp0s8 enp0s3
    nfs
      sources: 10.102.1.11/32 10.102.1.12/32
    ssh
      sources: 10.102.1.1/32

      [david@backup ~]$ sudo firewall-cmd --get-default-zone 
    drop

      [david@backup ~]$ sudo firewall-cmd --info-zone=ssh 
    ssh (active)
      target: default
      icmp-block-inversion: no
      interfaces: 
      sources: 10.102.1.1/32
      services: 
      ports: 
      protocols: 
      masquerade: no
      forward-ports: 
      source-ports: 
      icmp-blocks: 
      rich rules: 

      [david@backup ~]$ sudo firewall-cmd --info-zone=nfs
    nfs (active)
      target: default
      icmp-block-inversion: no
      interfaces: 
      sources: 10.102.1.11/32 10.102.1.12/32
      services: 
      ports: 2049/tcp 111/tcp 2049/udp 111/udp 
      protocols: 
      masquerade: no
      forward-ports: 
      source-ports: 
      icmp-blocks: 
      rich rules:   

      [david@backup ~]$ sudo firewall-cmd --list-all --zone=drop
    drop (active)
      target: DROP
      icmp-block-inversion: no
      interfaces: enp0s3 enp0s8
      sources: 
      services: 
      ports: 
      protocols: 
      masquerade: no
      forward-ports: 
      source-ports: 
      icmp-blocks: 
      rich rules:

### D. Reverse Proxy

🌞 **Restreindre l'accès au reverse proxy `front.tp2.linux`**

- seules les machines du réseau `10.102.1.0/24` doivent pouvoir joindre le proxy
- n'oubliez pas votre accès SSH

      [david@front ~]$ sudo firewall-cmd --set-default-zone=drop 
      success
   
      [david@front ~]$ sudo firewall-cmd --permanent --new-zone=ssh
      success
      [david@front ~]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent  
      success
      [david@bfront ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent 
      success

      [david@front ~]$ sudo firewall-cmd --permanent --new-zone=proxy
      success
      [david@front ~]$ sudo firewall-cmd --zone=proxy --add-port=443/tcp --permanent  
      [david@front ~]$ sudo firewall-cmd --zone=proxy --add-source=10.102.1.0/24 --permanent 
      success

      [david@front ~]$ sudo firewall-cmd --reload 
      success

🌞 **Montrez le résultat de votre conf avec une ou plusieurs commandes `firewall-cmd`**

      [david@front ~]$ sudo firewall-cmd --get-active-zones 
    drop
      interfaces: enp0s8 enp0s3
    proxy
      sources: 10.102.1.0/24
    ssh
      sources: 10.102.1.1/32

      [david@front ~]$ sudo firewall-cmd --get-default-zone 
    drop

      [david@front ~]$ sudo firewall-cmd --info-zone=ssh 
    ssh (active)
      target: default
      icmp-block-inversion: no
      interfaces: 
      sources: 10.102.1.1/32
      services: 
      ports: 
      protocols: 
      masquerade: no
      forward-ports: 
      source-ports: 
      icmp-blocks: 
      rich rules: 

      [david@front ~]$ sudo firewall-cmd --info-zone=proxy
    proxy (active)
      target: default
      icmp-block-inversion: no
      interfaces: 
      sources: 10.102.1.0/24 
      services: 
      ports: 443/tcp
      protocols: 
      masquerade: no
      forward-ports: 
      source-ports: 
      icmp-blocks: 
      rich rules: 

      [david@front ~]$ sudo firewall-cmd --list-all --zone=drop
    drop (active)
      target: DROP
      icmp-block-inversion: no
      interfaces: enp0s3 enp0s8
      sources: 
      services: 
      ports: 
      protocols: 
      masquerade: no
      forward-ports: 
      source-ports: 
      icmp-blocks: 
      rich rules:

### E. Tableau récap

🌞 **Rendez-moi le tableau suivant, correctement rempli :**

| Machine            | IP            | Service                 | Port ouvert       | IPs autorisées |
|--------------------|---------------|-------------------------|-------------------|----------------|
| `web.tp2.linux`    | `10.102.1.11` | Serveur Web             | 80/tcp            | 10.102.1.14/32 |
|                    |               |                         | 22/tcp            | 10.102.1.1/32  |
| `db.tp2.linux`     | `10.102.1.12` | Serveur Base de Données | 22/tcp            | 10.102.1.1/32  |
|                    |               |                         | 3306/tcp          | 10.102.1.11/32 |
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) | 22/tcp            | 10.102.1.1/32  |
|                    |               |                         | 111/tcp 2049/tcp  | 10.102.1.11/32 |
|                    |               |                         | 111/tcp 2049/tcp  | 10.102.1.12/32 |
|                    |               |                         | 111/udp 2049/udp  | 10.102.1.11/32 |
|                    |               |                         | 111/udp 2049/udp  | 10.102.1.12/32 |
| `front.tp2.linux`  | `10.102.1.14` | Reverse Proxy           | 22/tcp            | 10.102.1.1/32  |
|                    |               |                         | 443/tcp           | 10.102.1.0/24  |
