# TP2 pt. 1 : Gestion de service


# Sommaire

- [TP2 pt. 1 : Gestion de service](#tp2-pt-1--gestion-de-service)
- [Sommaire](#sommaire)
- [I. Un premier serveur web](#i-un-premier-serveur-web)
  - [1. Installation](#1-installation)
  - [2. Avancer vers la maîtrise du service](#2-avancer-vers-la-maîtrise-du-service)
- [II. Une stack web plus avancée](#ii-une-stack-web-plus-avancée)
  - [1. Intro](#1-intro)
  - [2. Setup](#2-setup)
    - [A. Serveur Web et NextCloud](#a-serveur-web-et-nextcloud)
    - [B. Base de données](#b-base-de-données)
    - [C. Finaliser l'installation de NextCloud](#c-finaliser-linstallation-de-nextcloud)



# I. Un premier serveur web

## 1. Installation

🖥️ **VM web.tp2.linux**

🌞 **Installer le serveur Apache**

    [david@web ~]$ sudo dnf install httpd -y

    [...]

    Installed:
      apr-1.6.3-11.el8.1.x86_64
      apr-util-1.6.1-6.el8.1.x86_64
      apr-util-bdb-1.6.1-6.el8.1.x86_64
      apr-util-openssl-1.6.1-6.el8.1.x86_64
      httpd-2.4.37-39.module+el8.4.0+571+fd70afb1.x86_64
      httpd-filesystem-2.4.37-39.module+el8.4.0+571+fd70afb1.noarch
      httpd-tools-2.4.37-39.module+el8.4.0+571+fd70afb1.x86_64
      mod_http2-1.15.7-3.module+el8.4.0+553+7a69454b.x86_64
      rocky-logos-httpd-84.5-8.el8.noarch

    Complete!
    [david@web ~]$



🌞 **Démarrer le service Apache**

Démarrage

    [david@web ~]$ sudo systemctl start httpd.service

Démarrage persistant

    [david@web ~]$ sudo systemctl enable httpd.service
    Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.

Ouverture port firewalld:

    [david@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
    success
    [david@web ~]$ sudo firewall-cmd --reload
    success
    [david@web ~]$
    
Utiliser une commande `ss` pour savoir sur quel port tourne actuellement Apache

    [david@web ~]$ sudo ss -alnpt
    
    [...]
    LISTEN              0                   128                                          *:80                                          *:*                  users:(("httpd",pid=4357,fd=4),("httpd",pid=4356,fd=4),
    [...]
    [david@web ~]$


🌞 **TEST**

vérifier que le service est démarré
    
    [david@web ~]$ sudo systemctl is-active httpd.service
    active

vérifier qu'il est configuré pour démarrer automatiquement

    [david@web ~]$ sudo systemctl is-enabled httpd.service
    enabled
    
vérifier avec une commande `curl localhost` que vous joignez votre serveur web localement

    [david@web ~]$ curl localhost
    <!doctype html>
    <html>
      <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>HTTP Server Test Page powered by: Rocky Linux</title>
        <style type="text/css">
          /*<![CDATA[*/

          html {
            height: 100%;
            width: 100%;
      [...]



vérifier avec votre navigateur (sur votre PC) que vous accéder à votre serveur web

    https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP2/Screenshot/port_80.png


## 2. Avancer vers la maîtrise du service

🌞 **Le service Apache...**

donnez la commande qui permet d'activer le démarrage automatique d'Apache quand la machine s'allume

    [david@web ~]$ sudo systemctl enable httpd.service

prouvez avec une commande qu'actuellement, le service est paramétré pour démarré quand la machine s'allume

        [david@web ~]$ sudo systemctl is-enabled httpd.service
    enabled

affichez le contenu du fichier `httpd.service` qui contient la définition du service Apache

    [david@web ~]$ vim /etc/systemd/system/multi-user.target.wants/httpd.service
    [...]

    [Unit]
    Description=The Apache HTTP Server
    Wants=httpd-init.service
    After=network.target remote-fs.target nss-lookup.target httpd-init.service
    Documentation=man:httpd.service(8)

    [Service]
    Type=notify
    Environment=LANG=C

    ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
    ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
    # Send SIGWINCH for graceful stop
    KillSignal=SIGWINCH
    KillMode=mixed
    PrivateTmp=true

    [Install]
    WantedBy=multi-user.target

🌞 **Déterminer sous quel utilisateur tourne le processus Apache**

mettez en évidence la ligne dans le fichier de conf qui définit quel user est utilisé

    [david@web ~]$ vim /etc/httpd/conf/httpd.conf
    [...]
    User apache
    [...]

utilisez la commande `ps -ef` pour visualiser les processus en cours d'exécution et confirmer que apache tourne bien sous l'utilisateur mentionné dans le fichier de conf

    [david@web ~]$ ps -ef  | grep httpd
    root        4353       1  0 16:39 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
    apache      4354    4353  0 16:39 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
    apache      4355    4353  0 16:39 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
    apache      4356    4353  0 16:39 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
    apache      4357    4353  0 16:39 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
    david       5106    3412  0 17:04 pts/0    00:00:00 grep --color=auto httpd
    

vérifiez avec un `ls -al` le dossier du site (dans `/var/www/...`) 
    
    [david@web ~]$ ls -al /var/www/html/
    total 0
    drwxr-xr-x. 2 root root  6 Jun 11 17:35 .
    drwxr-xr-x. 4 root root 33 Sep 29 16:35 ..

vérifiez que tout son contenu appartient à l'utilisateur mentionné dans le fichier de conf
  
      drwxr-xr-x. 2 root root  6 Jun 11 17:35 .  <--- les autres utilisateurs (dont apache) peuvent lire et executer le contenu du dossier de site  
    

🌞 **Changer l'utilisateur utilisé par Apache**

créez le nouvel utilisateur
    
    [david@web ~]$ sudo useradd -r -s /bin/nologin new_user

modifiez la configuration d'Apache pour qu'il utilise ce nouvel utilisateur

    [david@web ~]$ vim /etc/httpd/conf/httpd.conf
    [...]
    User new_user
    Group new_user
    [...]
    
redémarrez Apache
    
    [david@web ~]$ sudo systemctl restart httpd.service

utilisez une commande `ps` pour vérifier que le changement a pris effet

    [david@web ~]$ ps -ef | grep httpd
    root        5610       1  0 17:21 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
    new_user    5612    5610  0 17:21 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
    new_user    5613    5610  0 17:21 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
    new_user    5614    5610  0 17:21 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
    new_user    5615    5610  0 17:21 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
    david       5830    3412  0 17:22 pts/0    00:00:00 grep --color=auto httpd

🌞 **Faites en sorte que Apache tourne sur un autre port**

modifiez la configuration d'Apache pour lui demande d'écouter sur un autre port

    [david@web ~]$ sudo vim /etc/httpd/conf/httpd.conf
    [...]
    Listen 1234
    [...]

ouvrez un nouveau port firewall, et fermez l'ancien

    [david@web ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
    success
    [david@web ~]$ sudo firewall-cmd --add-port=1234/tcp --permanent
    success
    [david@web ~]$ sudo firewall-cmd --reload
    success


redémarrez Apache

    [david@web ~]$ sudo systemctl restart httpd.service
    [david@web ~]$

prouvez avec une commande `ss` que Apache tourne bien sur le nouveau port choisi

    [david@web ~]$ sudo ss -anltp
    [...]
    LISTEN              0                   128                                          *:1234                                        *:*                  users:(("httpd",pid=5977,fd=4),("httpd",pid=5976,fd=4),("httpd",pid=5975,fd=4),("httpd",pid=5972,fd=4))


vérifiez avec `curl` en local que vous pouvez joindre Apache sur le nouveau port

    [david@web ~]$ curl localhost:1234
    <!doctype html>
    [...]
    [david@web ~]$

vérifiez avec votre navigateur que vous pouvez joindre le serveur sur le nouveau port

    https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP2/Screenshot/port_1234.png

📁 **Fichier `/etc/httpd/conf/httpd.conf`** (que vous pouvez renommer si besoin, vu que c'est le même nom que le dernier fichier demandé)

    https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP2/Configuration/apache.conf

# II. Une stack web plus avancée

## 1. Intro

    david@web ~]$ sudo vim /etc/httpd/conf/httpd.conf
    [sudo] password for david:
    [...]
    IncludeOptional sites-enabled/*

## 2. Setup


### A. Serveur Web et NextCloud


📁 **Fichier `/etc/httpd/conf/httpd.conf`**  

    https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP2/Configuration/conf_apache_nextcloud

📁 **Fichier `/etc/httpd/conf/sites-available/web.tp2.linux`**

    https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP2/Configuration/web.tp2.linux

    [david@web ~]$ sudo dnf install epel-release -y
    Last metadata expiration check: 0:05:05 ago on Wed 06 Oct 2021 02:21:34 PM CEST.
    Dependencies resolved.
    ========================================================================================================================
     Package                         Architecture              Version                      Repository                 Size
    ========================================================================================================================
    Installing:
     epel-release                    noarch                    8-13.el8                     extras                     23 k

    Transaction Summary
    ========================================================================================================================
    Install  1 Package

    Total download size: 23 k
    Installed size: 35 k
    Downloading Packages:
    epel-release-8-13.el8.noarch.rpm                                                        117 kB/s |  23 kB     00:00
    ------------------------------------------------------------------------------------------------------------------------
    Total                                                                                    48 kB/s |  23 kB     00:00
    Running transaction check
    Transaction check succeeded.
    Running transaction test
    Transaction test succeeded.
    Running transaction
      Preparing        :                                                                                                1/1
      Installing       : epel-release-8-13.el8.noarch                                                                   1/1
      Running scriptlet: epel-release-8-13.el8.noarch                                                                   1/1
      Verifying        : epel-release-8-13.el8.noarch                                                                   1/1

    Installed:
      epel-release-8-13.el8.noarch

    Complete!

    [david@web ~]$ sudo dnf update -y
    Last metadata expiration check: 0:00:55 ago on Wed 06 Oct 2021 02:27:20 PM CEST.
    Dependencies resolved.
    Nothing to do.
    Complete!

        david@web ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm -y
    Last metadata expiration check: 0:04:31 ago on Wed 06 Oct 2021 02:27:20 PM CEST.
    remi-release-8.rpm                                                                      150 kB/s |  26 kB     00:00
    Dependencies resolved.
    ========================================================================================================================
     Package                      Architecture           Version                         Repository                    Size
    ========================================================================================================================
    Installing:
     remi-release                 noarch                 8.4-1.el8.remi                  @commandline                  26 k

    Transaction Summary
    ========================================================================================================================
    Install  1 Package

    Total size: 26 k
    Installed size: 20 k
    Downloading Packages:
    Running transaction check
    Transaction check succeeded.
    Running transaction test
    Transaction test succeeded.
    Running transaction
      Preparing        :                                                                                                1/1
      Installing       : remi-release-8.4-1.el8.remi.noarch                                                             1/1
      Verifying        : remi-release-8.4-1.el8.remi.noarch                                                             1/1

    Installed:
      remi-release-8.4-1.el8.remi.noarch

    Complete!

    [david@web ~]$ sudo dnf module enable php:remi-7.4 -y
    Last metadata expiration check: 0:03:51 ago on Wed 06 Oct 2021 02:32:54 PM CEST.
    Dependencies resolved.
    ========================================================================================================================
     Package                     Architecture               Version                       Repository                   Size
    ========================================================================================================================
    Enabling module streams:
     php                                                    remi-7.4

    Transaction Summary
    ========================================================================================================================

    Complete!
    
    [david@web ~]$ sudo systemctl is-enabled httpd
    enabled
    

    [david@web ~]$ sudo dnf install mariadb-server httpd vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp -y
    Last metadata expiration check: 0:10:06 ago on Wed 06 Oct 2021 02:32:54 PM CEST.
    Package httpd-2.4.37-39.module+el8.4.0+571+fd70afb1.x86_64 is already installed.
    Package vim-enhanced-2:8.0.1763-15.el8.x86_64 is already installed.
    Package zip-3.0-23.el8.x86_64 is already installed.
    Package unzip-6.0-45.el8_4.x86_64 is already installed.
    Package libxml2-2.9.7-9.el8_4.2.x86_64 is already installed.
    Package openssl-1:1.1.1g-15.el8_3.x86_64 is already installed.
    Dependencies resolved.
    ========================================================================================================================
     Package                              Architecture   Version                                    Repository         Size
    ========================================================================================================================
    Installing:
     php74-php                            x86_64         7.4.24-1.el8.remi                          remi-safe         1.5 M
     php74-php-bcmath                     x86_64         7.4.24-1.el8.remi                          remi-safe          87 k
     php74-php-common                     x86_64         7.4.24-1.el8.remi                          remi-safe         709 k
     php74-php-gd                         x86_64         7.4.24-1.el8.remi                          remi-safe          92 k
     php74-php-gmp                        x86_64         7.4.24-1.el8.remi                          remi-safe          84 k
     php74-php-intl                       x86_64         7.4.24-1.el8.remi                          remi-safe         201 k
     php74-php-json                       x86_64         7.4.24-1.el8.remi                          remi-safe          81 k
     php74-php-mbstring                   x86_64         7.4.24-1.el8.remi                          remi-safe         491 k
     php74-php-mysqlnd                    x86_64         7.4.24-1.el8.remi                          remi-safe         200 k
     php74-php-pdo                        x86_64         7.4.24-1.el8.remi                          remi-safe         130 k
     php74-php-pecl-zip                   x86_64         1.19.5-1.el8.remi                          remi-safe          57 k
     php74-php-process                    x86_64         7.4.24-1.el8.remi                          remi-safe          91 k
     php74-php-xml                        x86_64         7.4.24-1.el8.remi                          remi-safe         180 k
     wget                                 x86_64         1.19.5-10.el8                              appstream         733 k
    Installing dependencies:
     checkpolicy                          x86_64         2.9-1.el8                                  baseos            345 k
     environment-modules                  x86_64         4.5.2-1.el8                                baseos            420 k
     fontconfig                           x86_64         2.13.1-3.el8                               baseos            273 k
     gd                                   x86_64         2.2.5-7.el8                                appstream         143 k
     jbigkit-libs                         x86_64         2.1-14.el8                                 appstream          54 k
     libX11                               x86_64         1.6.8-4.el8                                appstream         610 k
     libX11-common                        noarch         1.6.8-4.el8                                appstream         157 k
     libXau                               x86_64         1.0.9-3.el8                                appstream          36 k
     libXpm                               x86_64         3.5.12-8.el8                               appstream          57 k
     libicu65                             x86_64         65.1-1.el8.remi                            remi-safe         9.3 M
     libjpeg-turbo                        x86_64         1.5.3-10.el8                               appstream         154 k
     libsodium                            x86_64         1.0.18-2.el8                               epel              162 k
     libtiff                              x86_64         4.0.9-18.el8                               appstream         187 k
     libwebp                              x86_64         1.0.0-3.el8_4                              appstream         271 k
     libxcb                               x86_64         1.13.1-1.el8                               appstream         228 k
     libxslt                              x86_64         1.1.32-6.el8                               baseos            249 k
     oniguruma5php                        x86_64         6.9.7.1-1.el8.remi                         remi-safe         210 k
     php74-libzip                         x86_64         1.8.0-1.el8.remi                           remi-safe          69 k
     php74-runtime                        x86_64         1.0-3.el8.remi                             remi-safe         1.1 M
     policycoreutils-python-utils         noarch         2.9-14.el8                                 baseos            251 k
     python3-audit                        x86_64         3.0-0.17.20191104git1c2f876.el8.1          baseos             85 k
     python3-libsemanage                  x86_64         2.9-6.el8                                  baseos            126 k
     python3-policycoreutils              noarch         2.9-14.el8                                 baseos            2.2 M
     python3-setools                      x86_64         4.3.0-2.el8                                baseos            625 k
     scl-utils                            x86_64         1:2.0.2-13.el8                             appstream          46 k
     tcl                                  x86_64         1:8.6.8-2.el8                              baseos            1.1 M
    Installing weak dependencies:
     php74-php-cli                        x86_64         7.4.24-1.el8.remi                          remi-safe         3.1 M
     php74-php-fpm                        x86_64         7.4.24-1.el8.remi                          remi-safe         1.6 M
     php74-php-opcache                    x86_64         7.4.24-1.el8.remi                          remi-safe         274 k
     php74-php-sodium                     x86_64         7.4.24-1.el8.remi                          remi-safe          87 k

    Transaction Summary
    ========================================================================================================================
    Install  44 Packages 
    [...]

    [david@web httpd]$ sudo vim /etc/httpd/sites-available/linux.tp2.web
    
    <VirtualHost *:80>
      DocumentRoot /var/www/sub-domains/linux.tp2.web/html/
      ServerName  web.tp2.linux

      <Directory /var/www/sub-domains/linux.tp2.web/html/>
        Require all granted
        AllowOverride All
        Options FollowSymLinks MultiViews

        <IfModule mod_dav.c>
          Dav off
        </IfModule>
      </Directory>
    </VirtualHost>
    
    [david@web ~]$ sudo mkdir /etc/httpd/sites-enabled/
    
    [david@web ~]$ sudo ln -s /etc/httpd/sites-available/linux.tp2.web /etc/httpd/sites-enabled/
    
    [david@web ~]$ sudo mkdir -p /var/www/sub-domains/linux.tp2.web/html/
    
    [david@web ~]$ timedatectl | grep Time
                Time zone: Europe/Paris (CEST, +0200)
     
    [david@web ~]$ sudo vim /etc/opt/remi/php74/php.ini
    date.timezone = "Europe/Paris"
    
    [david@web ~]$ ls -al /etc/localtime
    lrwxrwxrwx. 1 root root 34 Sep 15 15:29 /etc/localtime -> ../usr/share/zoneinfo/Europe/Paris
    
    [david@web ~]$ wget https://download.nextcloud.com/server/releases/nextcloud-22.2.0.zip
    [david@web ~]$ unzip nextcloud-22.2.0.zip
    
    [david@web ~]$cd nextcloud
    
    [david@web nextcloud]$ sudo mv * /var/www/sub-domains/linux.tp2.web/html/
    
    
    [david@web nextcloud]$ sudo chown -Rf apache.apache /var/www/sub-domains/linux.tp2.web/html/
[david@web nextcloud]$


    [david@web linux.tp2.web]$ sudo firewall-cmd --add-port=80/tcp --permanent
    success
    [david@web linux.tp2.web]$ sudo firewall-cmd --reload
    
    [david@web ~]$ sudo vim /etc/hosts
    [sudo] password for david:
    10.102.1.11 web.tp2.linux web    
    
    

### B. Base de données

    [david@db ~]$ sudo dnf install mariadb-server -y
    [david@db ~]$ sudo systemctl start mariadb.service
    [david@db ~]$ sudo systemctl enable mariadb.service
    Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
    Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
    Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
    
    [david@db ~]$ mysql_secure_installation

    NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
          SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

    In order to log into MariaDB to secure it, we'll need the current
    password for the root user.  If you've just installed MariaDB, and
    you haven't set the root password yet, the password will be blank,
    so you should just press enter here.

    Enter current password for root (enter for none):
    OK, successfully used password, moving on...

    Setting the root password ensures that nobody can log into the MariaDB
    root user without the proper authorisation.

    Set root password? [Y/n] y
    New password:
    Re-enter new password:
    Password updated successfully!
    Reloading privilege tables..
     ... Success!


    By default, a MariaDB installation has an anonymous user, allowing anyone
    to log into MariaDB without having to have a user account created for
    them.  This is intended only for testing, and to make the installation
    go a bit smoother.  You should remove them before moving into a
    production environment.

    Remove anonymous users? [Y/n] y
     ... Success!

    Normally, root should only be allowed to connect from 'localhost'.  This
    ensures that someone cannot guess at the root password from the network.

    Disallow root login remotely? [Y/n] y
     ... Success!

    By default, MariaDB comes with a database named 'test' that anyone can
    access.  This is also intended only for testing, and should be removed
    before moving into a production environment.

    Remove test database and access to it? [Y/n] y
     - Dropping test database...
     ... Success!
     - Removing privileges on test database...
     ... Success!

    Reloading the privilege tables will ensure that all changes made so far
    will take effect immediately.

    Reload privilege tables now? [Y/n] y
     ... Success!

    Cleaning up...

    All done!  If you've completed all of the above steps, your MariaDB
    installation should now be secure.

    Thanks for using MariaDB!

    [david@db ~]$sudo  ss -nalpt
    [...]
    LISTEN    0         80                       *:3306                    *:*        users:(("mysqld",pid=61102,fd=21))
    [...]
    
    [david@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
    success
    [david@db ~]$ sudo firewall-cmd --reload
    success
    
🌞 **Préparation de la base pour NextCloud**


    [david@db ~]$ sudo mysql -u root -p
        Enter password:
        [...]
    MariaDB [(none)]> Create User 'nextcloud'@'10.102.1.11' IDENTIFIED BY "meow";
    Query OK, 0 rows affected (0.000 sec)

    MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
    Query OK, 1 row affected (0.001 sec)

    MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
    Query OK, 0 rows affected (0.001 sec)

    MariaDB [(none)]> FLUSH PRIVILEGES;
    Query OK, 0 rows affected (0.001 sec)

    MariaDB [(none)]>


🌞 **Exploration de la base de données**

    [david@web ~]$ mysql -u nextcloud -h 10.102.1.12 -p
    Enter password:
    Welcome to the MariaDB monitor.  Commands end with ; or \g.
    Your MariaDB connection id is 10
    Server version: 10.3.28-MariaDB MariaDB Server

    Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

    Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

    MariaDB [(none)]> SHOW DATABASES;
    +--------------------+
    | Database           |
    +--------------------+
    | information_schema |
    | nextcloud          |
    +--------------------+
    2 rows in set (0.002 sec)

    MariaDB [(none)]> use nextcloud;
    Database changed
    MariaDB [nextcloud]> SHOW TABLES;
    Empty set (0.002 sec)
    
    MariaDB [(none)]> SELECT User FROM mysql.user;
    ERROR 1142 (42000): SELECT command denied to user 'nextcloud'@'web.tp2.linux' for table 'user'
    
Depuis db.tp2.linux:

    MariaDB [(none)]> SELECT User FROM mysql.user;
    +-----------+
    | User      |
    +-----------+
    | nextcloud |
    | root      |
    | root      |
    | root      |
    +-----------+
    

### C. Finaliser l'installation de NextCloud

🌞 sur votre PC

    https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP2/Screenshot/host_windows.png

🌞 **Exploration de la base de données**

    [david@db ~]$ sudo mysql -u root -p
    [sudo] password for david:
    Enter password:
    Welcome to the MariaDB monitor.  Commands end with ; or \g.
    Your MariaDB connection id is 141
    Server version: 10.3.28-MariaDB MariaDB Server

    Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

    Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

    MariaDB [(none)]> use nextcloud;
    Reading table information for completion of table and column names
    You can turn off this feature to get a quicker startup with -A

    Database changed
    MariaDB [nextcloud]> SELECT FOUND_ROWS();
    +--------------+
    | FOUND_ROWS() |
    +--------------+
    |          108 |
    +--------------+
    1 row in set (0.000 sec)
    
> Ce tableau devra figurer à la fin du rendu, avec les ? remplacés par la bonne valeur (un seul tableau à la fin). Je vous le remets à chaque fois, à des fins de clarté, pour lister les machines qu'on a à chaque instant du TP.

    [david@db ~]$ sudo firewall-cmd --add-source=10.102.1.11/32 --permanent
    [sudo] password for david:
    success
    [david@db ~]$ sudo firewall-cmd --reload
    success

    [david@web ~]$ sudo firewall-cmd --add-source=10.102.1.12/32 --permanent
    [sudo] password for david:
    success
    [david@web ~]$ sudo firewall-cmd --add-source=10.102.1.1/32 --permanent
    [david@web ~]$ sudo firewall-cmd --reload
    success
    
| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | 80          | 10.102.1.1 / 10.102.1.12 |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données | 3306        | 10.102.1.11 |
