#!/bin/bash

# script backup database
# David 12/10/21

# Fixe date
day=$(date +%y%m%d_%H%M%S)

# Backup name
backup=tp2_backup_db_$day.tar.gz

# Absolute path
destination=$(readlink -f $1)
archive=$(readlink -f $backup)
database_sql=$(readlink -f database.sql)


#Exctract database
mysqldump -u root $2 > database.sql


# Compress directory
tar -czvf $backup $database_sql

# Send backup
rsync -av --remove-source-files  $archive $destination

# Delete more than 5 backup files
cd $destination
ls -tQ  | tail -n+6 | xargs rm &>/dev/null
cd -
rm $database_sql

