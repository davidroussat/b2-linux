# TP3: Public Key Infrastructure

## Sommaire

- [TP3: Public Key Infrastructure](#TP3:-Public-Key-Infrastructure)
  - [Sommaire](#sommaire)
  - [0. Description](#0-Description)
  - [I. Prérequis](#I-Prérequis)
  - [II. Déploiement de l'infrastructure](#II-Déploiement-de-l'infrastructure)
  - [III. PKI](#III-PKI)
      - [1. CA](#1-ca)
      - [2. Intermediaire](#2-Intermediaire)
          - [Description des dossiers les plus importants](#2-description-des-dossiers-les-plus-importants)
          - [Solutions mises en place](#solutions-mises-en-place)
    - [3. OCSP](#3-ocsp)
        - [Test du service OCSP](#test-du-service-ocsp)
        - [Revocation de certificat à la main](#revocation-de-certificat-à-la-main)
  - [IV. Backup](#iv-backup)
    - [1. Backup du CA](#1-Backup-du-CA)
    - [2. Backup de l'intermédiaire](#2-Backup-de-l'intermédiaire)
    - [3. Backup OCSP](#3-backup-ocsp)
  - [V. Monitoring](#v-monitoring)



## 0. Description

La solution proposée permet de mettre en place une infrastructure autour d'une PKI:

- Une authorité de certification (CA)

    -> Elle délégue son rôle de signature de certificat à un serveur intémerdiaire.
    -> L'objectif est de la préserver au maximum 

- Un intermédiaire

    -> Il a toute la confiance du CA pour signer les certificats serveurs / clients
    
- Online Certificate Status Protocol

    -> Les clients viendront interroger ce serveur afin de s'arrurer de la validité des certificats

- Un serveur Ansible
    -> Déploiement de l'ensemble de l'infrastructure

- Un serveur Zabbix

    -> Monitoring de l'infrastructure

- Un serveur NFS

    -> Backup des fichiers sensibles
    

## I. Prérequis


- Installer 6 VMs Rocky Linux et créer un unique utilisateur au nom de David

- Répéter ces consignes sur les 6 hôtes EN TERMINANT PAR ANSIBLE:
    - SANS les droits SUDO:

        - Générer une paire de clé ssh sans définir de phrase de protection et en laissant le dossier par default
        
    - AVEC les droits SUDO:
    
        - Déployer un script config_{nom_hote}.sh" par hôte

        https://gitlab.com/davidroussat/b2-linux/-/tree/main/Config_h%C3%B4tes
    
- Concernant ZABBIX:

   -  déployer d'abord le scritp confi_zabbix.sh

    https://gitlab.com/davidroussat/b2-linux/-/blob/main/Config_h%C3%B4tes/config_zabbix.sh
   
   -  lancer la commande mysql_secure_installation
   -  Executer la commande "sudo mysql -u root -p" et taper les commandes suivantes: 

                [david@zabbix ~]$ sudo mysql -u root -p
            [sudo] password for david:
            Sorry, try again.
            [sudo] password for david:
            Enter password:
            Welcome to the MariaDB monitor.  Commands end with ; or \g.
            Your MariaDB connection id is 35
            Server version: 10.5.9-MariaDB MariaDB Server

            Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

            Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

            MariaDB [(none)]> CREATE DATABASE zabbix CHARACTER SET utf8 COLLATE utf8_bin;
            Query OK, 1 row affected (0.001 sec)

            MariaDB [(none)]>    GRANT ALL privileges ON zabbix.* TO zabbix@localhost IDENTIFIED BY 'supermdp';
            Query OK, 0 rows affected (0.001 sec)

            MariaDB [(none)]> FLUSH privileges;
            Query OK, 0 rows affected (0.001 sec)

            MariaDB [(none)]> exit
            Bye
        

    - Executer la commande suivante et donner comme mot de passe 'supermdp':

            [david@zabbix ~]$  zcat /usr/share/doc/zabbix-sql-scripts/mysql/create.sql.gz | mysql -uzabbix -p zabbix
            Enter password:
            [david@zabbix ~]$
            
                            

- Concernant l'hôte Ansible, le dernier script à lancer SANS droit sudo doit être "ssh.sh"
    
    https://gitlab.com/davidroussat/b2-linux/-/blob/main/Config_h%C3%B4tes/ssh.sh

# II. Déploiement de l'infrastructure:


Afin de déployer l'ensemble de la solution:

- Sur l'hôte "ansible"

    - Copier l'ensemble du dossier "ansible" et coller le dans le répertoire personnel de david

    https://gitlab.com/davidroussat/b2-linux/-/tree/main/TP3/ansible
    
    - Executer la commande suivante afin de créer un volume Logique et de le chiffrer à l'aide de "LUKS" sur les 3 briques de la PKI 
    
    https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP3/ansible/secure_env.yml

            [david@ansible ansible]$ ansible-playbook secure_env.yml -K
            BECOME password:

            PLAY [Mise en place de l'environnement] ********************************************************************************

            TASK [Mise en place de l'environement sécurisé] ************************************************************************
            changed: [10.2.1.13]
            changed: [10.2.1.10]
            changed: [10.2.1.11]

            TASK [Accept les nouvelles connexion ssh sans poser de question] *******************************************************
            changed: [10.2.1.10]
            changed: [10.2.1.11]
            changed: [10.2.1.13]

            PLAY RECAP *************************************************************************************************************
            10.2.1.10                  : ok=2    changed=2    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
            10.2.1.11                  : ok=2    changed=2    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
            10.2.1.13                  : ok=2    changed=2    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0


    - Executer la commande suivante afin de déployer la configuration des autres briques (backup / apache / zabbix) et la configuration ssh ( modifications de "StrictHostKeyChecking" avec la valeur "accept-new". Ce qui permet de se connecter sur la machine pour la 1ère fois sans demander de confirmation.):
    
    https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP3/ansible/post_install.yml

    https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP3/ansible/configuration/ssh_config.j2
       
            [david@ansible ansible]$ ansible-playbook post_install.yml -K
            BECOME password:

            PLAY [Préparation de l'environnement] **********************************************************************************
            [...]
            PLAY RECAP *************************************************************************************************************
            10.2.1.10                  : ok=8    changed=3    unreachable=0    failed=0    skipped=14   rescued=0    ignored=0
            10.2.1.11                  : ok=8    changed=3    unreachable=0    failed=0    skipped=14   rescued=0    ignored=0
            10.2.1.12                  : ok=7    changed=2    unreachable=0    failed=0    skipped=15   rescued=0    ignored=0
            10.2.1.13                  : ok=8    changed=3    unreachable=0    failed=0    skipped=14   rescued=0    ignored=0
            10.2.1.14                  : ok=13   changed=9    unreachable=0    failed=0    skipped=9    rescued=0    ignored=0
            10.2.1.20



    - Executer la commande suivante afin de déployer la configuration de la PKI
    
    https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP3/ansible/installation.yml
                
                [david@ansible ansible]$ ansible-playbook installation.yml

            PLAY [Préparation de l'environnement] *******************************************************************************************************************************************************

            TASK [include_tasks] ************************************************************************************************************************************************************************
            included: /home/david/ansible/ssh_generate.yml for 10.2.1.10, 10.2.1.11, 10.2.1.13, 10.2.1.20, 10.2.1.14, 10.2.1.12
                [...]
                PLAY RECAP **********************************************************************************************************************************************************************************
            10.2.1.10                  : ok=10   changed=6    unreachable=0    failed=0    skipped=4    rescued=0    ignored=0
            10.2.1.11                  : ok=6    changed=2    unreachable=0    failed=0    skipped=8    rescued=0    ignored=0
            10.2.1.12                  : ok=5    changed=2    unreachable=0    failed=0    skipped=9    rescued=0    ignored=0
            10.2.1.13                  : ok=7    changed=3    unreachable=0    failed=0    skipped=7    rescued=0    ignored=0
            10.2.1.14                  : ok=5    changed=1    unreachable=0    failed=0    skipped=9    rescued=0    ignored=0
            10.2.1.20                  : ok=6    changed=2    unreachable=0    failed=0    skipped=8    rescued=0    ignored=0

    - Executer la commande afin de déployer les services et timers créés:
    
    https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP3/ansible/deploy_service.yml

                        [david@ansible ansible]$ ansible-playbook deploy_service.yml -K
            BECOME password:

            PLAY [Déploiement des services] *********************************************************************************************************************************************************************************
            [...]
            PLAY RECAP **********************************************************************************************************************************************************************************
            10.2.1.10                  : ok=0    changed=0    unreachable=0    failed=0    skipped=6    rescued=0    ignored=0
            10.2.1.11                  : ok=2    changed=3    unreachable=0    failed=0    skipped=4    rescued=0    ignored=0
            10.2.1.12                  : ok=0    changed=0    unreachable=0    failed=0    skipped=6    rescued=0    ignored=0
            10.2.1.13                  : ok=2    changed=3    unreachable=0    failed=0    skipped=4    rescued=0    ignored=0
            10.2.1.14                  : ok=0    changed=0    unreachable=0    failed=0    skipped=6    rescued=0    ignored=0
            10.2.1.20                  : ok=2    changed=2    unreachable=0    failed=0    skipped=4    rescued=0    ignored=0



## III. PKI


### 1. CA

C'est l'hôte le plus important de la PKI. Il doit absolument être préservé de toutes attaques.  Il n'a plus d'intérêt d'être allumé car il a donné sa confiance à l'hôte intermédiaire pour signer les certificats. Il sera de nouveau employé uniquement si l'intermediaire a été compromis.


    [david@CA coffre]$ tree
    .
    ├── certs
    │   └── ca.cert.pem
    ├── crl
    ├── index.txt
    ├── index.txt.attr
    ├── index.txt.old
    ├── intermediate
    │   ├── apache
    │   │   ├── apache.crt
    │   │   └── apache.key.pem
    │   ├── certs
    │   │   ├── apache.crt
    │   │   ├── ca-chain.cert.pem
    │   │   ├── intermediate.cert.pem
    │   │   └── ocsp.cert.pem
    │   ├── crl
    │   ├── csr
    │   │   ├── apache.csr.pem
    │   │   ├── intermediate.csr.pem
    │   │   └── ocsp.csr.pem
    │   ├── index.txt
    │   ├── index.txt.attr
    │   ├── index.txt.attr.old
    │   ├── index.txt.old
    │   ├── newcerts
    │   │   ├── 1000.pem
    │   │   └── 1001.pem
    │   ├── openssl.cnf
    │   ├── private
    │   │   ├── apache.key.pem
    │   │   ├── intermediate.key.pem
    │   │   └── ocsp.key.pem
    │   ├── serial
    │   └── serial.old
    ├── lost+found
    ├── newcerts
    │   └── 1000.pem
    ├── ocsp
    │   ├── ca-chain.cert.pem
    │   ├── index.txt
    │   ├── ocsp.cert.pem
    │   └── ocsp.key.pem
    ├── openssl.cnf
    ├── private
    │   └── ca.key.pem
    ├── serial
    └── serial.old
    
Les commandes utilisées concernant la créations des certificats et clés privé sont présentes dans le script  "ca.sh"

https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP3/ansible/script/to_ca/ca.sh


Fichier de configuration du CA:

https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP3/ansible/configuration/openssl.j2

### 2. Intermediaire

C'est l'hôte qui signera tous les certificats clients et serveurs.

    [david@intermediate_web coffre]$ tree
    .
    ├── backup_intermediate.sh
    ├── intermediate
    │   ├── apache
    │   │   ├── apache.crt
    │   │   └── apache.key.pem
    │   ├── certs
    │   │   ├── apache.crt
    │   │   ├── ca-chain.cert.pem
    │   │   ├── intermediate.cert.pem
    │   │   └── ocsp.cert.pem
    │   ├── crl
    │   ├── csr
    │   │   ├── apache.csr.pem
    │   │   ├── intermediate.csr.pem
    │   │   └── ocsp.csr.pem
    │   ├── index.txt
    │   ├── index.txt.attr
    │   ├── index.txt.attr.old
    │   ├── index.txt.old
    │   ├── newcerts
    │   │   ├── 1000.pem
    │   │   ├── 1001.pem
    │   │   ├── 1002.pem
    │   │   └── 1003.pem
    │   ├── openssl.cnf
    │   ├── private
    │   │   ├── apache.key.pem
    │   │   ├── intermediate.key.pem
    │   │   └── ocsp.key.pem
    │   ├── serial
    │   └── serial.old
    ├── lost+found
    └── renew_certificate.sh

#### Description des dossiers les plus importants

- Fichier de configuration de l'intermediaire:

    https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP3/ansible/configuration/openssl_int.j2

- Le dossier "private" contient les clés privées générées

- Le fichier "index.txt" contient les informations sur les certificats en cours d'utilisation:

        [david@intermediate_web intermediate]$ cat index.txt
        V       221118152935Z           1000    unknown /C=FR/ST=France/O=secure.lab/CN=ocsp.secure.lab
        R       221118152936Z   211108153251Z   1001    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        R       221118153251Z   211108153252Z   1002    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        V       221118153252Z           1003    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        
        V = Valider
        R = Révoquer
    
- Le dossier "newscert" contient le numéro des certificats (validés ou non)

- Le dossier "csr" contient les certificats en attentes de signatures

- Le dossier "cert" contient les certificats signés et prêt à être utilisés

#### Solutions mises en place

- Script permettant de révoquer un certificat, en créer un autre, le signer et le déployer sur le serveur apache:
    
    https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP3/ansible/script/to_intermediate/renew_certificate.sh
    
    
        david@intermediate_web coffre]$ cat intermediate/index.txt
        V       221118152935Z           1000    unknown /C=FR/ST=France/O=secure.lab/CN=ocsp.secure.lab
        R       221118152936Z   211108153251Z   1001    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        R       221118153251Z   211108153252Z   1002    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        V       221118153252Z           1003    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
    
        [david@intermediate_web coffre]$ ./renew_certificate.sh
        Using configuration from intermediate/openssl.cnf
        Revoking Certificate 1003.
        Data Base Updated
        Using configuration from intermediate/openssl.cnf
        Check that the request matches the signature
        Signature ok
        Certificate Details:
                Serial Number: 4100 (0x1004)
                Validity
                    Not Before: Nov  8 16:50:57 2021 GMT
                    Not After : Nov 18 16:50:57 2022 GMT
                Subject:
                    countryName               = FR
                    stateOrProvinceName       = France
                    organizationName          = secure.lab
                    commonName                = apache.secure.lab
                X509v3 extensions:
                    X509v3 Basic Constraints:
                        CA:FALSE
                    Netscape Cert Type:
                        SSL Server
                    Netscape Comment:
                        OpenSSL Generated Server Certificate
                    X509v3 Subject Key Identifier:
                        E6:91:AD:A6:95:0D:38:87:C0:FB:9C:D5:5D:1C:DF:07:D2:EA:6B:99
                    X509v3 Authority Key Identifier:
                        keyid:76:90:B2:8A:BE:2F:36:45:29:F4:EF:40:E6:F0:FE:A9:35:07:ED:1C
                        DirName:/C=FR/ST=France/O=secure.lab/CN=CA.secure.lab
                        serial:10:00

                    X509v3 Key Usage: critical
                        Digital Signature, Key Encipherment
                    X509v3 Extended Key Usage:
                        TLS Web Server Authentication
                    Authority Information Access:
                        OCSP - URI:http://10.2.1.13

        Certificate is to be certified until Nov 18 16:50:57 2022 GMT (375 days)

        Write out database with 1 new entries
        Data Base Updated
        Warning: Permanently added '10.2.1.20' (ECDSA) to the list of known hosts.
        sending incremental file list
        ./
        apache.crt
        apache.key.pem

        sent 1,800 bytes  received 111 bytes  3,822.00 bytes/sec
        total size is 5,520  speedup is 2.89
        Warning: Permanently added '10.2.1.13' (ECDSA) to the list of known hosts.
        sending incremental file list
        index.txt

        sent 546 bytes  received 41 bytes  1,174.00 bytes/sec
        total size is 437  speedup is 0.74
        
        
        [david@intermediate_web coffre]$ cat intermediate/index.txt
        [...]
        R       221118153252Z   211108165057Z   1003    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        V       221118165057Z           1004    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
 
- Service permettant d'exécuter le script automatiquement:
    
    https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP3/ansible/script/to_intermediate/renew_certificate.service
     
         [david@intermediate_web coffre]$ sudo systemctl restart renew_certificate.service 2>/dev/null
        [sudo] password for david:
        
        [david@intermediate_web coffre]$ cat intermediate/index.txt
        [...]
        R       221118165057Z   211108165311Z   1004    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        V       221118165311Z           1005    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
     
- Timer permettant d'exécuter le script tous les jours:
    
    https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP3/ansible/script/to_intermediate/renew_certificate.timer

        [david@intermediate_web coffre]$ sudo systemctl restart renew_certificate.timer
        
        [david@intermediate_web coffre]$ cat intermediate/index.txt
        [...]
        R       221118165311Z   211108165359Z   1005    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        V       221118165359Z           1006    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        
        [david@intermediate_web coffre]$ ls intermediate/newcerts/
            1000.pem  1001.pem  1002.pem  1003.pem  1004.pem  1005.pem  1006.pem
        
        
### 3. OCSP

C'est le serveur qui renseignera les clients sur la validité des certificats utilisée par les serveurs, sur lesquelles ils effetueront leurs requêtes.
 
    [david@ocsp coffre]$ tree
    .
    ├── backup_ocsp.sh
    ├── lost+found
    ├── ocsp
    │   ├── ca-chain.cert.pem
    │   ├── index.txt
    │   ├── log.txt
    │   ├── ocsp.cert.pem
    │   └── ocsp.key.pem
    ├── start_ocsp.sh
    └── stop_ocsp.sh

    2 directories, 8 files
    
Toutes les révocations précédentes faites à l'aide du script "renew_certificate.sh" ont mises à jour le fichier "index.txt" du serveur OCSP, qui peut ainsi informer les clients sur l'état du certificat utilisé par le serveur apache:  

        [david@ocsp coffre]$ cat ocsp/index.txt
        [...]
        R       221118165311Z   211108165359Z   1005    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        V       221118165359Z           1006    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab

- Le fichier log.txt contiendra l'historique des requêtes effectuées par les clients


- Le démarrage du service s'effectue grâce au script suivant avec les droit sudo:
    
    https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP3/ansible/script/to_ocsp/start_ocsp.sh

        [david@ocsp coffre]$ sudo ./start_ocsp.sh
        ocsp: waiting for OCSP client connections...

- L'arrêt du service s'effectue grâce au script suivant avec les droit sudo:
    
    https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP3/ansible/script/to_ocsp/stop_ocsp.sh

        [david@ocsp coffre]$ sudo ./stop_ocsp.sh
        Terminated

- Le démarrage automatique du service s'effectue grâce service suivant:
    
    https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP3/ansible/script/to_ocsp/start_ocsp.service

        [david@ocsp coffre]$ sudo systemctl restart start_ocsp.service
        
- L'arrêt automatique du service s'effectue grâce service suivant:
    
    https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP3/ansible/script/to_ocsp/stop_ocsp.service

        [david@ocsp coffre]$ sudo systemctl restart stop_ocsp.service

- Le démarrage s'effectue tous les jours grâce au timer suivant:
    
    https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP3/ansible/script/to_ocsp/start_ocsp.timer

        [david@ocsp coffre]$ sudo systemctl restart start_ocsp.timer

- L'arrêt s'effectue tous les jours grâce au timer suivant:
    
    https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP3/ansible/script/to_ocsp/stop_ocsp.timer

        [david@ocsp coffre]$ sudo systemctl restart stop_ocsp.timer

#### Test du service OCSP

- Le client qui tentera de faire une requête vers le serveur apache aura alors une alerte de sécurité

- Afin d'enlever cette aleerte, il convient d'installer dans le magasin de cetification du poste client, le certificat de la chaine de confiance. C'est à dire le certificat du CA et de l'intermediaire. Nous avons plus tôt concaténer les 2 certificats en un seul:

        [david@ansible script]$ vim to_ca/ca.sh
        [...]
         # Chain-file certificat
        cat intermediate/certs/intermediate.cert.pem \
        certs/ca.cert.pem > intermediate/certs/ca-chain.cert.pem
        [...]
        
- Copie de la chaine de confiance sur le poste client avec l'extension ".cer" pour windows:

        PS C:\Users\rouss> scp -r  david@10.2.1.11:/mnt/coffre/intermediate/certs/ca-chain.cert.pem C:\Users\rouss\OneDrive\Bureau\ca-chain.cer
        david@10.2.1.11's password:
        ca-chain.cert.pem
        
- Intégration du certificat dans le magasin de certification de notre navigateur préféré:

    1- Rentrer dans les paramètres:

    https://gitlab.com/davidroussat/b2-linux/-/blob/main/Screenshot/Screenshot_2.png

    2- Rechercher "certificat":

    https://gitlab.com/davidroussat/b2-linux/-/blob/main/Screenshot/Screenshot_1.png

    3- Dans l'onglet "autorités", importer le certificat et lui faire confiance pour identifez les sites web :

    https://gitlab.com/davidroussat/b2-linux/-/blob/main/Screenshot/Screenshot_5.png
    https://gitlab.com/davidroussat/b2-linux/-/blob/main/Screenshot/Screenshot_6.png

    La configuration d'apache nous permet de rediriger les requêtes sur le port 80 vers le 443:

    https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP3/ansible/configuration/ssl.j2

    4- Requête du client sur le port 80 vers le serveur apache:

    https://gitlab.com/davidroussat/b2-linux/-/blob/main/Screenshot/Screenshot_7.png

    Aucune alerte de sécurité n'est remontée:

    https://gitlab.com/davidroussat/b2-linux/-/blob/main/Screenshot/Screenshot_9.png


- Démarrage du service OCSP:

        [david@ocsp coffre]$ sudo ./start_ocsp.sh
        ocsp: waiting for OCSP client connections...

- Redémarrage d'apache car le certificat a été révoqués       
        
        [david@apache ~]$ sudo systemctl restart httpd.service
        
- Requête du client vers le serveur apache et vérification sur le serveur OCSP:

        
        ocsp: waiting for OCSP client connections...
            OCSP Request Data:
            Version: 1 (0x0)
            Requestor List:
                Certificate ID:
                  Hash Algorithm: sha1
                  Issuer Name Hash: 65EE6E2B8A1FE694F1EBF9EB733DE812A2CA5A81
                  Issuer Key Hash: 7690B28ABE2F364529F4EF40E6F0FEA93507ED1C
                  Serial Number: 100D
        OCSP Response Data:
            OCSP Response Status: successful (0x0)
            Response Type: Basic OCSP Response
            Version: 1 (0x0)
            Responder Id: C = FR, ST = France, O = secure.lab, CN = ocsp.secure.lab
            Produced At: Nov  8 17:45:04 2021 GMT
            Responses:
            Certificate ID:
              Hash Algorithm: sha1
              Issuer Name Hash: 65EE6E2B8A1FE694F1EBF9EB733DE812A2CA5A81
              Issuer Key Hash: 7690B28ABE2F364529F4EF40E6F0FEA93507ED1C
              Serial Number: 100D
            Cert Status: good  <---
            This Update: Nov  8 17:45:04 2021 GMT
            Next Update: Nov  8 17:46:04 2021 GMT
            
#### Revocation de certificat  à la main

- Révocation par l'intermédiaire:

        [david@intermediate_web coffre]$ openssl ca -config intermediate/openssl.cnf \
                    -revoke intermediate/apache/apache.crt
        Using configuration from intermediate/openssl.cnf
        Revoking Certificate 100D.
        Data Base Updated
    
- Synchronisation avec l'OCSP:

        [david@intermediate_web coffre]$ rsync -av intermediate/index.txt david@10.2.1.13:/mnt/coffre/ocsp/index.txt
        sending incremental file list
        index.txt

        sent 775 bytes  received 47 bytes  1,644.00 bytes/sec
        total size is 1,363  speedup is 1.66

- Redémarrage du service httpd:

        [david@apache ~]$ sudo systemctl restart httpd.service

- Lancement du service OCSP:

        [david@ocsp coffre]$ sudo openssl ocsp -nmin 1 -index ocsp/index.txt -port 80 -CA ocsp/ca-chain.cert.pem -text -rkey ocsp/ocsp.key.pem  -rsigner ocsp/ocsp.cert.pem 
        
- Nouvelle requête du client:

    https://gitlab.com/davidroussat/b2-linux/-/blob/main/Screenshot/Screenshot_11.png


- Résultat sur le serveur OCSP:

        ocsp: waiting for OCSP client connections...
        OCSP Request Data:
        Version: 1 (0x0)
        Requestor List:
            Certificate ID:
              Hash Algorithm: sha1
              Issuer Name Hash: 65EE6E2B8A1FE694F1EBF9EB733DE812A2CA5A81
              Issuer Key Hash: 7690B28ABE2F364529F4EF40E6F0FEA93507ED1C
              Serial Number: 100D <---- Numéro du certificat
        OCSP Response Data:
            OCSP Response Status: successful (0x0)
            Response Type: Basic OCSP Response
            Version: 1 (0x0)
            Responder Id: C = FR, ST = France, O = secure.lab, CN = ocsp.secure.lab
            Produced At: Nov  8 18:00:17 2021 GMT
            Responses:
            Certificate ID:
              Hash Algorithm: sha1
              Issuer Name Hash: 65EE6E2B8A1FE694F1EBF9EB733DE812A2CA5A81
              Issuer Key Hash: 7690B28ABE2F364529F4EF40E6F0FEA93507ED1C
              Serial Number: 100D
            Cert Status: revoked <----- Certificat révoqué! 
            Revocation Time: Nov  8 17:54:32 2021 GMT
            This Update: Nov  8 18:00:17 2021 GMT
            Next Update: Nov  8 18:01:17 2021 GMT
            [...]

- Vérificaiton du numéro de certificat:

        [david@ocsp coffre]$ cat ocsp/index.txt
        [...]
        R       221118171033Z   211108175432Z   100D    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        [...]
        
## IV. Backup

La sauvegarde des fichiers est effectuée sur un serveur NFS
qui partage un dossier distinct pour le CA, l'intermédiaire et l'OCSP.

    [david@backup ~]$ cat /etc/exports
    /srv/backup/ca 10.2.1.10(rw,no_root_squash)
    /srv/backup/intermediate 10.2.1.11(rw,no_root_squash)
    /srv/backup/ocsp 10.2.1.13(rw,no_root_squash)
    
### 1. Backup du CA

Le backup du CA est effectué à la fin du déploiement de la PKI par le serveur ansible avec le playbook deploy_pki.yml, dans le script "backup_ca.sh"
    
https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP3/ansible/script/to_ca/backup_ca.sh
https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP3/ansible/deploy_pki.yml
    
        [david@ansible ansible]$ vim deploy_pki.yml
        [...]
          - name : backup du CA
        script : script/to_ca/backup_ca.sh /srv/backup /mnt/coffre
        when: ansible_host == "10.2.1.10"
        [...]
        
Pour rappel, ce serveur est destiné à être éteind. C'est la raison pour laquelle je n'ai pas prévu de timer afin d'automatiser d'éventuelle sauvegarde

- Vérifications

        [david@backup ~]$ cd /srv/backup/ca/
        [david@backup ca]$ ls
        tp2_backup_211108_162937.tar.gz

        [david@backup ca]$ tar -xf tp2_backup_211108_162937.tar.gz

        [david@backup ca]$ cd mnt/coffre/
        [david@backup coffre]$ ls
        certs  index.txt       index.txt.old  lost+found  ocsp         private  serial.old
        crl    index.txt.attr  intermediate   newcerts    openssl.cnf  serial
    
### 2. Backup de l'intermédiaire:


- script de backup:
    
    https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP3/ansible/script/to_intermediate/backup_intermediate.sh

        [david@intermediate_web coffre]$ date
        Mon Nov  8 19:19:14 CET 2021
        [david@intermediate_web coffre]$ ./backup_intermediate.sh /srv/backup/ intermediate/index.txt
        tar: Removing leading `/' from member names
        /mnt/coffre/intermediate/index.txt
        sending incremental file list
        tp2_backup_211108_191931.tar.gz

        sent 510 bytes  received 43 bytes  1,106.00 bytes/sec
        total size is 379  speedup is 0.69
        /mnt/coffre

- service exécutant script de backup automatiquement:
    
    https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP3/ansible/script/to_intermediate/backup_intermediate.service

        [david@intermediate_web coffre]$ sudo systemctl restart backup_intermediate.service
        [sudo] password for david:

- timer exécutant le backup tous les jours:
    
    https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP3/ansible/script/to_intermediate/backup_intermediate.timer

        [david@intermediate_web coffre]$ sudo systemctl restart backup_intermediate.timer

- Vérifications

        [david@backup coffre]$ cd /srv/backup/intermediate/
        [david@backup intermediate]$ ls
        tp2_backup_211108_191931.tar.gz  tp2_backup_211108_192117.tar.gz
        tp2_backup_211108_192106.tar.gz

        david@backup intermediate]$ tar xzf tp2_backup_211108_191931.tar.gz

        [david@backup intermediate]$ cat mnt/coffre/intermediate/index.txt
        V       221118152935Z           1000    unknown /C=FR/ST=France/O=secure.lab/CN=ocsp.secure.lab
        R       221118152936Z   211108153251Z   1001    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        R       221118153251Z   211108153252Z   1002    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        R       221118153252Z   211108165057Z   1003    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        R       221118165057Z   211108165311Z   1004    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        R       221118165311Z   211108165359Z   1005    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        R       221118165359Z   211108170515Z   1006    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        R       221118170515Z   211108170542Z   1007    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        R       221118170542Z   211108170722Z   1008    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        R       221118170722Z   211108170735Z   1009    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        R       221118170735Z   211108170827Z   100A    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        R       221118170827Z   211108170948Z   100B    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        R       221118170948Z   211108171033Z   100C    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        R       221118171033Z   211108175432Z   100D    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        V       221118174001Z           100E    unknown /C=FR/ST=France/O=secure.lab/CN=client_apache


### 3. Backup OCSP

- script permettant de sauvegarder les données:
    
    https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP3/ansible/script/to_ocsp/backup_ocsp.sh

        [david@ocsp coffre]$ ./backup_ocsp.sh /srv/backup/ ocsp/index.txt
        tar: Removing leading `/' from member names
        /mnt/coffre/ocsp/index.txt
        sending incremental file list
        tp2_backup_211108_192630.tar.gz

        sent 504 bytes  received 43 bytes  1,094.00 bytes/sec
        total size is 374  speedup is 0.68
        /mnt/coffre
        
        [david@ocsp coffre]$ date
        Mon Nov  8 19:26:36 CET 2021

- service exécutant script de backup automatiquement:
    
    https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP3/ansible/script/to_ocsp/backup_ocsp.service

        [david@ocsp coffre]$ sudo systemctl restart backup_ocsp.service

- timer exécutant le backup tous les jours:
    
    https://gitlab.com/davidroussat/b2-linux/-/blob/main/TP3/ansible/script/to_ocsp/backup_ocsp.timer

        [david@ocsp coffre]$ sudo systemctl restart backup_ocsp.timer
   
- Vérifications

        [david@backup intermediate]$ cd /srv/backup/ocsp/
        [david@backup ocsp]$ ls
        tp2_backup_211108_163304.tar.gz  tp2_backup_211108_192630.tar.gz  tp2_backup_211108_192749.tar.gz
        tp2_backup_211108_163305.tar.gz  tp2_backup_211108_192741.tar.gz
    
        [david@backup ocsp]$ tar xzf tp2_backup_211108_192630.tar.gz

        [david@backup ocsp]$ cat mnt/coffre/ocsp/index.txt
        V       221118152935Z           1000    unknown /C=FR/ST=France/O=secure.lab/CN=ocsp.secure.lab
        R       221118152936Z   211108153251Z   1001    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        R       221118153251Z   211108153252Z   1002    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        R       221118153252Z   211108165057Z   1003    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        R       221118165057Z   211108165311Z   1004    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        R       221118165311Z   211108165359Z   1005    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        R       221118165359Z   211108170515Z   1006    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        R       221118170515Z   211108170542Z   1007    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        R       221118170542Z   211108170722Z   1008    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        R       221118170722Z   211108170735Z   1009    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        R       221118170735Z   211108170827Z   100A    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        R       221118170827Z   211108170948Z   100B    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        R       221118170948Z   211108171033Z   100C    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        R       221118171033Z   211108175432Z   100D    unknown /C=FR/ST=France/O=secure.lab/CN=apache.secure.lab
        V       221118174001Z           100E    unknown /C=FR/ST=France/O=secure.lab/CN=client_apache

## V. Monitoring

Après avoir déployé le serveur zabbix, il convient d'entrer l'IP du serveur dans notre navigateur pour finaliser sa configuration:

https://gitlab.com/davidroussat/b2-linux/-/blob/main/Screenshot/Screenshot_12.png
https://gitlab.com/davidroussat/b2-linux/-/blob/main/Screenshot/Screenshot_13.png
https://gitlab.com/davidroussat/b2-linux/-/blob/main/Screenshot/Screenshot_14.png

Nous pouvons à présent nous connecter à l'interface graphique en utilisant pour la première connexion le nom et mot de passe par défault
"Admin -> zabbix"

https://gitlab.com/davidroussat/b2-linux/-/blob/main/Screenshot/Screenshot_15.png

Interface graphique de zabbix:

https://gitlab.com/davidroussat/b2-linux/-/blob/main/Screenshot/Screenshot_16.png


Du côté des clients monitorés, j'ai inclus dans leur script de configuration, l'installation de l'agent zabbix et ouvert le port TCP 10050

https://gitlab.com/davidroussat/b2-linux/-/tree/main/Config_h%C3%B4tes

Du côté serveur, il reste à présent à indiquer les hôtes que nous souhaitons monitorer:

https://gitlab.com/davidroussat/b2-linux/-/blob/main/Screenshot/Screenshot_17.png

Indiquer obligatoirement un nom, un groupe, une interface (correspondant à l'adresse IP de l'hôte):

https://gitlab.com/davidroussat/b2-linux/-/blob/main/Screenshot/Screenshot_20.png
https://gitlab.com/davidroussat/b2-linux/-/blob/main/Screenshot/Screenshot_19.png

Il reste enfin à lui indiquer un "template" correspondant à la méthode utilisée par les hôtes pour envoyer leurs informations:

https://gitlab.com/davidroussat/b2-linux/-/blob/main/Screenshot/Screenshot_18.png

Nous pouvons à présent avoir des informations en temps réélle sur les serveurs monitorés:

https://gitlab.com/davidroussat/b2-linux/-/blob/main/Screenshot/Screenshot_21.png
