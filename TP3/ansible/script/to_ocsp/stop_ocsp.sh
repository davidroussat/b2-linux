#!/bin/bash
# Force l'arrêt du service ocsp
# David 07/11/21

# Kill ocsp service
ps -aux | grep ocsp | grep /bin/bash |  awk '{print $2}'  > ocsp.pid && sudo kill $(cat ocsp.pid)

rm ocsp.pid
