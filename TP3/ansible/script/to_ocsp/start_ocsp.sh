#!/bin/bash
# Démarrage du service ocsp
# David 07/11/21

cd /mnt/coffre

# Start service
openssl ocsp -nmin 1 -index ocsp/index.txt -port 80 -CA ocsp/ca-chain.cert.pem -text -rkey ocsp/ocsp.key.pem  -rsigner ocsp/ocsp.cert.pem -out ocsp/log.txt
