#!/bin/bash
# Revocation et renouvellement de certificat
# David 07/11/2021

cd /mnt/coffre

# Revoke certificate
openssl ca -config intermediate/openssl.cnf \
	        -revoke intermediate/apache/apache.crt

rm -f intermediate/apache/apache.crt
rm -f intermediate/certs/apache.crt
rm -f intermediate/csr/apache.csr.pem

# Certificate no signed with an existing key
openssl req -config intermediate/openssl.cnf \
	         -key intermediate/apache/apache.key.pem \
	         -new -sha256 -out intermediate/csr/apache.csr.pem \
		 -subj "/C=FR/ST=France/O=secure.lab/CN=apache.secure.lab"

# Signed by intermediate
openssl ca -batch -config intermediate/openssl.cnf \
	             -extensions server_cert -days 375 -notext -md sha256 \
	             -in intermediate/csr/apache.csr.pem \
	             -out intermediate/certs/apache.crt

cp intermediate/certs/apache.crt intermediate/apache
rsync -av intermediate/apache/ david@10.2.1.20:/etc/httpd/apache
rsync -av intermediate/index.txt david@10.2.1.13:/mnt/coffre/ocsp/index.txt
