#!/bin/bash
# Déploiement d'une PKI
# Roussat David ~ 07/11/2021


cd /mnt/coffre 

mkdir certs crl newcerts private
#chmod 700 private
touch index.txt
echo 1000 > serial

# Private Key
openssl genrsa -out private/ca.key.pem 4096
#chmod 400 private/ca.key.pem

# Certificate self-signed
openssl req -config openssl.cnf \
	    -key private/ca.key.pem \
	    -new -x509 -days 7300 -sha256 -extensions v3_ca \
	    -out certs/ca.cert.pem \
	    -subj "/C=FR/ST=France/O=secure.lab/CN=CA.secure.lab"

# Intermediate
mkdir -p intermediate/{certs,crl,csr,newcerts,private}
cd intermediate
#chmod 700 private
touch index.txt
echo 1000 > serial

cd ..

# Private key
openssl genrsa -out intermediate/private/intermediate.key.pem 4096
#chmod 400 intermediate/private/intermediate.key.pem

# Certificat no signed
openssl req -config intermediate/openssl.cnf -new -sha256 \
            -key intermediate/private/intermediate.key.pem \
            -out intermediate/csr/intermediate.csr.pem \
	    -subj "/C=FR/ST=France/O=secure.lab/CN=intermediate_web.secure.lab"

# Signed by CA
openssl ca -batch -config openssl.cnf -extensions v3_intermediate_ca \
	                -days 3650 -notext -md sha256 \
		        -in intermediate/csr/intermediate.csr.pem \
			-out intermediate/certs/intermediate.cert.pem
#chmod 444 intermediate/certs/intermediate.cert.pem

# Chain-file certificat
cat intermediate/certs/intermediate.cert.pem \
	certs/ca.cert.pem > intermediate/certs/ca-chain.cert.pem
#chmod 444 intermediate/certs/ca-chain.cert.pem


#OCSP

# Private key
openssl genrsa -out intermediate/private/ocsp.key.pem 4096

# Certificate no signed
openssl req -config intermediate/openssl.cnf -new -sha256 \
	    -key intermediate/private/ocsp.key.pem \
	    -out intermediate/csr/ocsp.csr.pem \
	    -subj "/C=FR/ST=France/O=secure.lab/CN=ocsp.secure.lab"

# Signed by intermediate
openssl ca -batch -config intermediate/openssl.cnf \
	   -extensions ocsp -days 375 -notext -md sha256 \
	   -in intermediate/csr/ocsp.csr.pem \
	   -out intermediate/certs/ocsp.cert.pem

mkdir ocsp
cp intermediate/certs/ca-chain.cert.pem ocsp/
cp intermediate/certs/ocsp.cert.pem ocsp/
cp intermediate/private/ocsp.key.pem ocsp/
cp intermediate/index.txt ocsp/


# APACHE cert

# Private key
openssl genrsa -out intermediate/private/apache.key.pem 4096

# Certificate no signed
openssl req -config intermediate/openssl.cnf \
          -key intermediate/private/apache.key.pem \
          -new -sha256 -out intermediate/csr/apache.csr.pem \
          -subj "/C=FR/ST=France/O=secure.lab/CN=apache.secure.lab"

# Signed by intermediate
openssl ca -batch -config intermediate/openssl.cnf \
                  -extensions server_cert -days 375 -notext -md sha256 \
                  -in intermediate/csr/apache.csr.pem \
                  -out intermediate/certs/apache.crt

mkdir intermediate/apache
cp intermediate/certs/apache.crt intermediate/apache
cp intermediate/private/apache.key.pem intermediate/apache

scp -r intermediate 10.2.1.11:/mnt/coffre
scp -r ocsp 10.2.1.13:/mnt/coffre
rsync -av intermediate/apache/ 10.2.1.20:/etc/httpd/apache

#/mnt/coffre/backup_ca.sh  
