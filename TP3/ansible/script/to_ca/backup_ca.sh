#!/bin/bash

#Script Backup
# Roussat David ~ 07/11/2021

# Fixe date
day=$(date +%y%m%d_%H%M%S)

# Backup name
backup=tp2_backup_$day.tar.gz

# Absolute path
source_file=$(readlink -f $2)
destination=$(readlink -f $1)
archive=$(readlink -f $backup)

# Compress directory
tar -czvf $backup $source_file

# Send backup
rsync -av --remove-source-files  $archive $destination;


