#!/bin/bash

pvcreate /dev/sdb
vgcreate vg_backup /dev/sdb
lvcreate -L 5g vg_backup -n lv_backup

openssl genrsa -out coffre.key 2048

cryptsetup -q --key-size=256 luksFormat /dev/mapper/vg_backup-lv_backup coffre.key
cryptsetup luksOpen --key-file coffre.key /dev/mapper/vg_backup-lv_backup coffre
mke2fs -t ext4 -L coffre /dev/mapper/coffre

mkdir /mnt/coffre
mount /dev/mapper/coffre /mnt/coffre/
chown -R david /mnt/coffre/
