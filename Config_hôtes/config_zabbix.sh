#!/bin/bash
# Prérequis
# David 07/11/21

# Install necessary packets
dnf update -y
dnf install nfs-utils -y
dnf install epel-release -y
rpm -Uvh https://repo.zabbix.com/zabbix/5.5/rhel/8/x86_64/zabbix-release-5.5-1.el8.noarch.rpm
dnf install zabbix-server-mysql zabbix-web-mysql zabbix-apache-conf zabbix-sql-scripts zabbix-selinux-policy zabbix-agent httpd mariadb-server php nmap -y

dnf module disable mariadb:10.3 -y
dnf module enable mariadb:10.5 -y
dnf module install mariadb:10.5 -y

systemctl enable --now mariadb

# Configuration zabbix file
sed -i 's/^max_execution_time.*/max_execution_time=600/' /etc/php.ini
sed -i 's/^max_input_time.*/max_input_time=600/' /etc/php.ini
sed -i 's/^memory_limit.*/memory_limit=256M/' /etc/php.ini
sed -i 's/^post_max_size.*/post_max_size=32M/' /etc/php.ini
sed -i 's/^upload_max_filesize.*/upload_max_filesize=16M/' /etc/php.ini
sed -i "s/^\;date.timezone.*/date.timezone=\'Europe\/Paris\'/" /etc/php.ini

echo "zabbix  ALL=(root) NOPASSWD: /usr/bin/nmap" >/etc/sudoers.d/zabbix

# Configuration réseau
nmcli connection modify enp0s8 ipv4.method manual connection.autoconnect yes ipv4.addresses 10.2.1.12/24 ipv4.dns 1.1.1.1
nmcli connection reload 
nmcli connection up enp0s8

# COnfiguration hostname
hostnamectl set-hostname zabbix.secure.lab

# COnfiguration firewalld
firewall-cmd --remove-service=cockpit --permanent
firewall-cmd --remove-service=dhcpv6-client --permanent
firewall-cmd --reload
firewall-cmd --add-port=80/tcp --permanent
firewall-cmd --add-port=10050/tcp --permanent
firewall-cmd --add-port=10051/tcp --permanent

# Configuration hôtes
echo "10.2.1.10	CA.secure.lab	CA" >> /etc/hosts	
echo "10.2.1.11	intermediate_web.secure.lab	intermediate_web" >> /etc/hosts	
echo "10.2.1.13	ocsp.secure.lab	ocsp" >> /etc/hosts	
echo "10.2.1.14	backup.secure.lab	backup" >> /etc/hosts	
echo "10.2.1.20	apache.secure.lab	apache" >> /etc/hosts	

systemctl enable --now zabbix-agent
systemctl enable --now zabbix-server
systemctl enable --now httpd
systemctl daemon-reload
