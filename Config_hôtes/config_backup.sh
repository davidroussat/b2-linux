#!/bin/bash
# Prérequis
# David 07/11/21

# Install necessary packets
dnf update -y
dnf install nfs-utils -y
dnf install epel-release -y

rpm -Uvh https://repo.zabbix.com/zabbix/5.5/rhel/8/x86_64/zabbix-release-5.5-1.el8.noarch.rpm
dnf install zabbix-agent -y
sed -i 's/^Server=127.0.0.1/Server=10.2.1.12/g' /etc/zabbix/zabbix_agentd.conf
sed -i 's/^ServerActive=127.0.0.1/ServerActive=10.2.1.12/g' /etc/zabbix/zabbix_agentd.conf
sed -i 's/^Hostname=Zabbix server/Hostname=backup/g' /etc/zabbix/zabbix_agentd.conf

systemctl enable --now zabbix-agent

systemctl start nfs-server.service
systemctl enable nfs-server.service

# Configuration network
nmcli connection modify enp0s8 ipv4.method manual connection.autoconnect yes ipv4.addresses 10.2.1.14/24 ipv4.dns 1.1.1.1
nmcli connection reload 
nmcli connection up enp0s8

# COnfiguratino hostname
hostnamectl set-hostname backup.secure.lab

# COnfiguraiton firewalld
firewall-cmd --remove-service=cockpit --permanent
firewall-cmd --remove-service=dhcpv6-client --permanent
firewall-cmd --add-service={nfs,nfs3,rpc-bind,mountd} --permanent
firewall-cmd --add-port=10050/tcp --permanent
sudo firewall-cmd --reload

# Configuration hosts
echo "10.2.1.11 intermediate_web.secure.lab     intermediate_web" >> /etc/hosts
echo "10.2.1.13 ocsp.secure.lab ocsp" >> /etc/hosts
echo "10.2.1.10 CA.secure.lab       CA" >> /etc/hosts
echo "10.2.1.20 apache.secure.lab       apache" >> /etc/hosts
echo "10.2.1.12 zabbix.secure.lab       zabbix" >> /etc/hosts

systemctl daemon-reload
