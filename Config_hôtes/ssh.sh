#!/bin/bash
# copie de la clé publique sur tous les hôtes de l'infra
# David 07/11/21

for host in '10.2.1.10' '10.2.1.11' '10.2.1.12' '10.2.1.13' '10.2.1.14' '10.2.1.20'
do
	ssh-copy-id -i ~/.ssh/id_rsa.pub $host
done
